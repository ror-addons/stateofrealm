SORData = {}

SORData.BarColours = {
-- Realm 
	[111] = {r = 255, g = 204, b = 65},
	[112] = {r = 79, g = 161, b = 77},
	[121] = {r = 240, g = 24, b = 85},
	[122] = {r = 182, g = 182, b = 182},
	[131] = {r = 185, g = 228, b = 250},
	[132] = {r = 185, g = 118, b = 237},
-- Classic
	[211] = {r =  60, g = 100, b = 200},
	[212] = {r = 255, g = 0, b = 0},
	[221] = {r = 60, g = 100, b = 200},
	[222] = {r = 255, g = 0, b = 0},
	[231] = {r = 60, g = 100, b = 200},
	[232] = {r = 255, g = 0, b = 0},
-- Emerald
	[311] = {r = 195, g = 229, b = 189},
	[312] = {r = 64, g = 152, b = 49},
	[321] = {r = 195, g = 229, b = 189},
	[322] = {r = 64, g = 152, b = 49},
	[331] = {r = 195, g = 229, b = 189},
	[332] = {r = 64, g = 152, b = 49},
-- Nightmare
	[411] = {r = 181, g = 198, b = 179},
	[412] = {r = 93, g = 127, b = 87},
	[421] = {r = 250, g = 248, b = 232},
	[422] = {r = 72, g = 74, b = 92},
	[431] = {r = 191, g = 183, b = 200},
	[432] = {r = 84, g = 37, b = 133},
-- Vampire
	[511] = {r = 239, g = 238, b = 228},
	[512] = {r = 133, g = 39, b = 79},
	[521] = {r = 239, g = 238, b = 228},
	[522] = {r = 133, g = 39, b = 79},
	[531] = {r = 239, g = 238, b = 228},
	[532] = {r = 133, g = 39, b = 79},
-- Amethyst
	[611] = {r = 190, g = 169, b = 225},
	[612] = {r = 102, g = 76, b = 145},
	[621] = {r = 190, g = 169, b = 225},
	[622] = {r = 102, g = 76, b = 145},
	[631] = {r = 190, g = 169, b = 225},
	[632] = {r = 102, g = 76, b = 145},
-- Eggplant
	[711] = {r = 202, g = 223, b = 194},
	[712] = {r = 100, g = 59, b = 129},
	[721] = {r = 202, g = 223, b = 194},
	[722] = {r = 100, g = 59, b = 129},
	[731] = {r = 202, g = 223, b = 194},
	[732] = {r = 100, g = 59, b = 129},
-- Pure
	[811] = {r = 155, g = 239, b = 159},
	[812] = {r = 28, g = 129, b = 32},
	[821] = {r = 254, g = 170, b = 71},
	[822] = {r = 223, g = 62, b = 24},
	[831] = {r = 187, g = 253, b = 255},
	[832] = {r = 57, g = 128, b = 209},	
	
}

SORData.LowMouseOvers = {
	["SOR.Realm1.Low.1Label"] = { 2, 1, 5099, 5098, 1, 1},
	["SOR.Realm1.Low.2Label"] = { 57, 61, 62, 135, 1, 2},
	["SOR.Realm1.Low.3Label"] = { 90, 100, 76, 101, 1, 3},
	
	["SOR.Realm2.Low.1Label"] = { 228, 229, 5097, 5096, 2, 1},
	["SOR.Realm2.Low.2Label"] = { 241, 239, 240, 242, 2, 2},
	["SOR.Realm2.Low.3Label"] = { 5090, 5094, 5091, 5092, 2, 3},

	["SOR.Realm3.Low.1Label"] = { 5072, 5073, 5075, 5074, 3, 1},
	["SOR.Realm3.Low.2Label"] = { 5070, 5071, 5068, 5069, 3, 2},
	["SOR.Realm3.Low.3Label"] = { 5066, 5065, 5063, 5064, 3, 3}
}

SORData.KeeptoLabel = {
["SOR.Realm1.Low.Keep2a.Icon"] = { level = "low", window = "SOR.Realm1.Low.2Label"	},
["SOR.Realm1.Low.Keep2b.Icon"] = { level = "low", window = "SOR.Realm1.Low.2Label"	},
["SOR.Realm1.Low.Keep3a.Icon"] = { level = "low", window = "SOR.Realm1.Low.3Label"	},
["SOR.Realm1.Low.Keep3b.Icon"] = { level = "low", window = "SOR.Realm1.Low.3Label"	},

["SOR.Realm2.Low.Keep2a.Icon"] = { level = "low", window = "SOR.Realm2.Low.2Label"	},
["SOR.Realm2.Low.Keep2b.Icon"] = { level = "low", window = "SOR.Realm2.Low.2Label"	},
["SOR.Realm2.Low.Keep3a.Icon"] = { level = "low", window = "SOR.Realm2.Low.3Label"	},
["SOR.Realm2.Low.Keep3b.Icon"] = { level = "low", window = "SOR.Realm2.Low.3Label"	},
	
["SOR.Realm3.Low.Keep2a.Icon"] = { level = "low", window = "SOR.Realm3.Low.2Label"	},
["SOR.Realm3.Low.Keep2b.Icon"] = { level = "low", window = "SOR.Realm3.Low.2Label"	},
["SOR.Realm3.Low.Keep3a.Icon"] = { level = "low", window = "SOR.Realm3.Low.3Label"	},
["SOR.Realm3.Low.Keep3b.Icon"] = { level = "low", window = "SOR.Realm3.Low.3Label"	},
	
	["SOR.Realm1.High.Keep4a.Icon"] = { level = "high", window = "SOR.Realm1.High.4Label"	},
	["SOR.Realm1.High.Keep4b.Icon"] = { level = "high", window = "SOR.Realm1.High.4Label"	},

	["SOR.Realm2.High.Keep4a.Icon"] = { level = "high", window = "SOR.Realm2.High.4Label"	},
	["SOR.Realm2.High.Keep4b.Icon"] = { level = "high", window = "SOR.Realm2.High.4Label"	},

	["SOR.Realm3.High.Keep4a.Icon"] = { level = "high", window = "SOR.Realm3.High.4Label"	},
	["SOR.Realm3.High.Keep4b.Icon"] = { level = "high", window = "SOR.Realm3.High.4Label"	},
	
}

SORData.HighMouseOvers = {
	["SOR.Realm1.High.4Label"] = {
	[2] = {153,158,157,156} ,
	[3] = {150,147,182,151} ,
	[4] = {161,160,162,159} ,
	["pairing"] = 1 ,
	} ,
	["SOR.Realm2.High.4Label"] = {
	[2] = {5086,5087,5085,5088} ,
	[3] = {5080,5084,5081,5083} ,
	[4] = {5077,5078,5076,5079}	 ,
	["pairing"] = 2 ,
	} ,
	["SOR.Realm3.High.4Label"] = {
	[2] = {632,635,634,633} ,
	[3] = {612,615,614,613} ,
	[4] = {636,639,638,637} ,
	["pairing"] = 3 ,
	} ,
}

SORData.Channel = {
	[1] = L"SoRorder",
	[2] = L"SoRdestr"
}