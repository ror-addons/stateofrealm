SORStringsEN = {}

SORStringsEN.KeepNames = {
  [1]=L"Dok Karaz",
  [2]=L"Fangbreaka Swamp",
  [3]=L"Gnol Baraz",
  [4]=L"Thickmuck Pit",
  [5]=L"Karaz Drengi",
  [6]=L"Kazad Dammaz",
  [7]=L"Bloodfist Rock",
  [8]=L"Karak Karag",
  [9]=L"Ironskin Skar",
  [10]=L"Badmoon Hole",

  [11]=L"Mandred's Hold",
  [12]=L"Stonetroll Keep",
  [13]=L"Passwatch Castle",
  [14]=L"Stoneclaw Castle",
  [15]=L"Wilhelm's Fist",
  [16]=L"Morr's Repose",
  [17]=L"Southern Garrison",
  [18]=L"Garrison of Skulls",
  [19]=L"Zimmeron's Hold",
  [20]=L"Charon's Keep",


  [21]=L"Cascades of Thunder",
  [22]=L"Spite's Reach",
  [23]=L"Well of Qhaysh",
  [24]=L"Ghrond's Sacristy",
  [25]=L"Arbor of Light",
  [26]=L"Pillars of Remembrence",
  [27]=L"Covenant of Flame",
  [28]=L"Drakebreaker's Scourge",
  [29]=L"Hatred's Way",
  [30]=L"Wrath's Resolve"
}

SORStringsEN.KeepMap = {
  [1]=L"Barak Varr",
  [2]=L"Marshes of Madness",
  [3]=L"Black Fire Pass",
  [4]=L"The Badlands",
  [5]=L"Kadrin Valley",
  [6]=L"Kadrin Valley",
  [7]=L"Thunder Mountain",
  [8]=L"Thunder Mountain",
  [9]=L"Black Crag",
  [10]=L"Black Crag",

  [11]=L"Ostland",
  [12]=L"Troll Country",
  [13]=L"Talabecland",
  [14]=L"High Pass",
  [15]=L"Reikland",
  [16]=L"Reikland",
  [17]=L"Praag",
  [18]=L"Praag",
  [19]=L"Chaos Wastes",
  [20]=L"Choas Wastes",


  [21]=L"Ellyrion",
  [22]=L"The Shadowlands",
  [23]=L"Saphery",
  [24]=L"Avelorn",
  [25]=L"Eataine",
  [26]=L"Eataine",
  [27]=L"Dragonwake",
  [28]=L"Dragonwake",
  [29]=L"Caledor",
  [30]=L"Caledor"
}


SORStringsEN.KeepAttackStatus = {
  [1]=L"Keep is Safe.",
  [2]=L"Outer Walls Under Attack!",
  [3]=L"Inner Walls Under Attack!",
  [4]=L"Keep Under Attack!",
  [5]=L"Keep Taken!"
}

	
SORStringsEN.Side = {
	[0]=L"No-one",
	[1]=L"Order",
	[2]=L"Destruction",
	[3]=L"No-one",
}

SORStringsEN.Opposition = {
	[0]=L"both sides",
	[1]=L"Destruction",
	[2]=L"Order",
	[3]=L"both sides",
}

SORStringsEN.OBJBoon = {
	[1] = L"Artisan's Gift",
	[2] = L"Merchant's Gift",
	[3] = L"Defensive Boon",
	[4] = L"Healing Boon"
}

SORStringsEN.Pairings = {
	[1] = L"Dwarf vs Orc",
	[2] = L"Empire vs Chaos",
	[3] = L"Elf vs Dark Elf"
}

-- Taken from the luas

SORStringsEN.Chats = {
  broadcast = L"Broadcast",
  _to = L" to ",
  warband = L"Warband",
  party = L"Party",
  region = L"1: Region",
  rvr = L"2: RvR",
  send_target = L"Send Target",
  broadcast_info = L"Broadcast Info",
  send_info = L"Send info",
  declare_attack = L"Declare Attack",
  declare_defence = L"Declare Defence",
}

SORStringsEN.SOR = {
  toggle_the_state_of_the_realm = L"Toggle the State of the Realm",
}

SORStringsEN.SORObjs = {
  contested_by = L"Contested by ",
  will_be_locked_down_by = L" will be locked down by " ,
  _in = L" in ",
  locked_by = L"Locked by ",
  is_locked_by = L" is locked by ",
  for_another = L" for another ",
  minutes = L"",
}

SORStringsEN.SOROptions = {
	Version = {
	  Title = L"SoR Version Notes",
	  VersionNo = {
	    label = L"State Of Realm 3 Beta"
	  },
	  Text1 = L"Hey there State of Realm users, Welcome to the \n" ..
	    L"State of Realms v3 Beta. This version gives you \n" ..
	    L"some of the functionality you have been asking \n" .. 
	    L"for.",
	  Text2 = L"Now copies of SoR on the same server will \n" ..
	    L"communicate information between themselves, \n" ..
	    L"this means that when you enter a zone with \n" .. 
	    L"someone already in, the data in your Pager will \n"..
	    L"automatically update.",
  	Text3 = L"Also the Objective Tracker will light up for BOs \n" .. 
	    L"in all zones, (if there is someone there to see \n" ..
	    L"the change). The Options window also has the\n" ..
	    L"Option to turn off the Objective Trackers for \n" ..
	    L"Tiers your not interested in.",
	  Text6 = L"I Hope you enjoy this new evolution in SoR. \n" ..
	    L"Yours Ratsug."
  },

  Title = L"State of Realm",
  Realm = {
    label = L"Realm Tracker"
  },
  RealmMode = {
    label = L"Realm Mode",
    DL1 = L"Hidden",
    DL2 = L"All Tiers",
    DL3 = L"Just Tier 4",
    DL4 = L"Tier 1,2,3"
  },
  SwingTime = {
    label = L"Zone Control Swing Timed Over",	
    info = L"01:00"
  },
  SwingLabel1 = L"1 min",
  SwingLabel2 = L"5 mins",
  SwingLabel3 = L"15 mins",
  Obj = {
    label = L"Objective Tracker"
  },
  ObjMode = {
    label = L"Objective Mode",
    DL1 = L"Hidden",
    DL2 = L"Docked",
    DL3 = L"Undocked"
  },
  GrowUp = {
    label = L"Grow Bars",
    DL1 = L"Up",
    DL2 = L"Down"
  },
	TierLabel = L"Tiers",
  Tier1 = {
    label = L"1"
  },
  Tier2 = {
    label = L"2"
  },
  Tier3 = {
    label = L"3"
  },
  Tier4 = {
    label = L"4"
  },
  Pager = {
    label = L"Pager"
  },
  PagerMode = {
    label = L"Pager Mode",
    DL1 = L"Hidden",
    DL2 = L"Docked",
    DL3 = L"Undocked"
  },
  Layout = {
    label = L"Layout"
  },
  ToolTip = {
    label = L"ToolTip Align",
    DL1 = L"Left",
    DL2 = L"Right"
  },
  ButtonMode = {
    label = L"Button Mode",
    DL1 = L"Normal",
    DL2 = L"Minimap"
  },
  AnchorWindow = {
    label = L"Anchor to",
    DL1 = L"Top",
    DL2 = L"Bottom"
  },
	ActiveUpdate = {
	  label = L"Active Update"
	},
	UpdateOn = {
	  label = L"Update Channel",
		DL1 = L"On",
		DL2 = L"Off"
  },
  PosReset = {
    label = L"Reset to Default Size & Position"
  },
  Look = {
    label = L"Overall Look"
  },
  Skin = {
    label = L"Colour Scheme",
    DL1 = L"Realm",
    DL2 = L"Classic",
		DL3 = L"Emerald",
		DL4 = L"Nightmare",
		DL5 = L"Vampire",
		DL6 = L"Amethyst",
		DL7 = L"EggPlant",
		DL8 = L"Pure"
  },
  Skin2 = {
    label = L"SoR Skin",
		DL1 = L"Original",
		DL2 = L"Cutdown",
		DL3 = L"Custom" 
  },
  Language = {
    label = L"Select Language",
		DL1 = L"English",
		DL2 = L"German"
  },
  Text = {
    label = L"Text Colour"
  },
  TextR = {
    label = L"Red"
  },
  TextG = {
    label = L"Green"
  },
  TextB = {
    label = L"Blue"
  },
  Tier = {
    label = L"Tier Colour"
  },
  TierR = {
    label = L"Red"
  },
  TierG = {
    label = L"Green"
  },
  TierB = {
    label = L"Blue"
  },
  AlphaLabel = {
    label = L"Background Opacity"
  },
  AlphaVal = {
    label = L"Alpha"
  },
  Save = L"Save",
  Reset = L"Reset",	
  Broadcast = {
    label = L"Broadcast Options"
  },
  Custom = {
    label = L"Custom Channel",
    sub = L"eg. g, ao, say"
  }
}

SORStringsEN.SORPager = {
  ChatData_match = L"Slow down!",
  Boons = {
    AG = L"AG",
    MG = L"MG",
    DB = L"DB",
    HB = L"HB"
  },
  controlled_by = L"Controlled by ",
  not_controlled = L"Not Controlled",
  open_for_capture = L"Open for Capture",
  locked = L"Locked",
  contested = L"Contested",
  status_unknown = L"Status Unknown",
  is_controlled_by = L" is controlled by ",
  but_i_dont_have_a_timer_on_it = L" but I don't have a timer on it.",
  no_bos_in_this_zone = L"No BOs in this zone",
  open_for_attack_by_the_forces_of = L" Open for attack by the forces of ",
  is_open_for_attack_by_the_forces_of = L"is open for attack by the forces of ",
  will_be_locked_down_by = L" will be locked down by ",
  _in = L" in ",
  is_locked_by = L" is locked by ",
  for_another = L" for another ",
  moving_on_to = L" Moving on to ",
  _next = L" next",
  minutes = L"",
}

SORStringsEN.SORRealms = {
  under_siege = L"Under Siege",
  altdorf = L"Altdorf",
  altdorf_taken = L"Altdorf Taken",
  inevitable_city = L"Inevitable City",
  inevitable_city_taken = L"Inevitable City Taken",
  safe = L"Safe",
  locked_down = L"Locked Down",
  the = L"The ",
  is_locked = L" is Locked",
  realm_taken = L"Realm Taken",
  has_been_taken_by = L" has been taken by ",
  is_under_attack_by = L" is under attack by ",
  has = L" has ",
  percent_control_of_zone = L"% control of zone",
  with_a = L"with a ",
  percent_swing_to = L"% swing to ",
  percent_swing_from = L"% swing from ",
  no_change = L"No Change",
  in_the_last = L"in the last ",
  percent_control_of = L"% control of ",
  percent_control = L"% control",
  locked_by = L"Locked by ",
  contested = L"Contested",
  not_owned = L"Not Owned",
  controlled_by = L"Controlled by ",
  heading_to = L"Heading to ",
  to_take = L" to take ",
  from = L" from ",
  to_defend = L" to defend ",
  from_an_attack_by = L" from an attack by ",
  _in = L" in ",
  is_under_attack_by_the_forces_of = L" is under attack by the forces of ",
  is_controlled_by = L" is controlled by ",
  and_is_safe = L" and is safe",
  siege_will_last_for_another = L" siege will last for another ",
  is_in_this_stage_for_another = L" is in this stage for another ",
  is_resetting_in_another = L" is resetting in another ",
  minutes = L"",
}

SORStringsEN.Info = {
	[2] = {name = L"The Lookout", objtype = 1, tier = 1, pair = 1, map = L"Mount Bloodhorn", zone = 0 },
	[1] = {name = L"Cannon Battery", objtype = 2, tier = 1, pair = 1, map = L"Ekrund", zone = 0 },
	[5099] = {name = L"Stonemine Tower", objtype = 3, tier = 1, pair = 1, map = L"Ekrund", zone = 0 },
	[5098] = {name = L"Ironmane Outpost", objtype = 4, tier = 1, pair = 1, map = L"Mount Bloodhorn", zone = 0 },
	
	[228] = {name = L"The Nordland XI", objtype = 1, tier = 1, pair = 2, map = L"Nordland", zone = 0 },
	[229] = {name = L"Lost Lagoon", objtype = 2, tier = 1, pair = 2, map = L"Norsca", zone = 0 },
	[5097] = {name = L"Festenplatz", objtype = 3, tier = 1, pair = 2, map = L"Nordland", zone = 0 },
	[5096] = {name = L"The Harvest Shrine", objtype = 4, tier = 1, pair = 2, map = L"Nordland", zone = 0 },

	[5072] = {name = L"The Shard of Grief", objtype = 1, tier = 1, pair = 3, map = L"Chrace", zone = 0 },
	[5073] = {name = L"The Tower of Nightflame", objtype = 2, tier = 1, pair = 3, map = L"Chrace", zone = 0 },
	[5075] = {name = L"The Altar of Khaine", objtype = 3, tier = 1, pair = 3, map = L"The Blighted Isle", zone = 0 },
	[5074] = {name = L"The House of Lorendyth", objtype = 4, tier = 1, pair = 3, map = L"The Blighted Isle", zone = 0 },	
	
	
	[57] = {name = L"The Lighthouse", objtype = 1, tier = 2, pair = 1, map = L"Barak Varr", zone = 0 },
	[61] = {name = L"Goblin Armory", objtype = 2, tier = 2, pair = 1, map = L"Marshes of Madness", zone = 0 },
	[62] = {name = L"Alcadizaar's Tomb", objtype = 3, tier = 2, pair = 1, map = L"Marshes of Madness", zone = 0 },
	[135] = {name = L"The Ironclad", objtype = 4, tier = 2, pair = 1, map = L"Barak Varr", zone = 0 },
	
	[241] = {name = L"Crypt of Weapons", objtype = 1, tier = 2, pair = 2, map = L"Ostland", zone = 0 },
	[239] = {name = L"Ruins of Greystone Keep", objtype = 2, tier = 2, pair = 2, map = L"Troll Country", zone = 0 },
	[240] = {name = L"Monastery of Morr", objtype = 3, tier = 2, pair = 2, map = L"Troll Country", zone = 0 },
	[242] = {name = L"Kinschel's Stronghold", objtype = 4, tier = 2, pair = 2, map = L"Ostland", zone = 0 },
	
	[5070] = {name = L"The Shadow Spire", objtype = 1, tier = 2, pair = 3, map = L"The ShadowlandS", zone = 0 },
	[5071] = {name = L"The Unicorn Siege Camp", objtype = 2, tier = 2, pair = 3, map = L"The ShadowlandS", zone = 0 },
	[5068] = {name = L"The Reaver Stables", objtype = 3, tier = 2, pair = 3, map = L"Ellyrion", zone = 0 },
	[5069] = {name = L"The Needle of Ellyrion", objtype = 4, tier = 2, pair = 3, map = L"Ellyrion", zone = 0 },
	
	
	[90] = {name = L"Karagaz", objtype = 1, tier = 3, pair = 1, map = L"The Badlands", zone = 0 },
	[100] = {name = L"Bugman's Brewery", objtype = 2, tier = 3, pair = 1, map = L"Black Fire Pass", zone = 0 },
	[76] = {name = L"Furrig's Fall", objtype = 3, tier = 3, pair = 1, map = L"Black Fire Pass", zone = 0 },
	[101] = {name = L"Goblin Artillery Range", objtype = 4, tier = 3, pair = 1, map = L"The Badlands", zone = 0 },
	
	[5090] = {name = L"Hallenfurt Manor", objtype = 1, tier = 3, pair = 2, map = L"High Pass", zone = 0 },
	[5094] = {name = L"Verentane's Tower", objtype = 2, tier = 3, pair = 2, map = L"Talabecland", zone = 0 },
	[5091] = {name = L"Feiten's Lock", objtype = 3, tier = 3, pair = 2, map = L"High Pass", zone = 0 },
	[5092] = {name = L"Ogrund's Tavern", objtype = 4, tier = 3, pair = 2, map = L"High Pass", zone = 0 },
	
	[5066] = {name = L"The Wood Choppaz", objtype = 1, tier = 3, pair = 3, map = L"Avelorn", zone = 0 },
	[5065] = {name = L"Maiden's Landing", objtype = 2, tier = 3, pair = 3, map = L"Avelorn", zone = 0 },
	[5063] = {name = L"Sari' Daroir", objtype = 3, tier = 3, pair = 3, map = L"Saphery", zone = 0 },
	[5064] = {name = L"The Spire of Teclis", objtype = 4, tier = 3, pair = 3, map = L"Saphery", zone = 0 },
	
	
	
	[153] = {name = L"Hardwater Falls", objtype = 1, tier = 4, pair = 1, map = L"Kadrin Valley", zone = 2 },
	[158] = {name = L"Dolgrund's Cairn", objtype = 2, tier = 4, pair = 1, map = L"Kadrin Valley", zone = 2 },
	[157] = {name = L"Gromril Junction", objtype = 3, tier = 4, pair = 1, map = L"Kadrin Valley", zone = 2 },
	[156] = {name = L"Icehearth Crossing", objtype = 4, tier = 4, pair = 1, map = L"Kadrin Valley", zone = 2 },
	
	[150] = {name = L"Doomstriker Vein", objtype = 1, tier = 4, pair = 1, map = L"Thunder Mountain", zone = 3 },
	[147] = {name = L"Karak Palik", objtype = 2, tier = 4, pair = 1, map = L"Thunder Mountain", zone = 3 },
	[182] = {name = L"Thargrim's Headwall", objtype = 3, tier = 4, pair = 1, map = L"Thunder Mountain", zone = 3 },
	[151] = {name = L"Gromril Kruk", objtype = 4, tier = 4, pair = 1, map = L"Thunder Mountain", zone = 3 },

	[161] = {name = L"Madcap Pickins", objtype = 1, tier = 4, pair = 1, map = L"Black Crag", zone = 4 },
	[160] = {name = L"Lobba Mill", objtype = 2, tier = 4, pair = 1, map = L"Black Crag", zone = 4 },
	[162] = {name = L"Rottenpike Ravine", objtype = 3, tier = 4, pair = 1, map = L"Black Crag", zone = 4 },
	[159] = {name = L"Squiggly Beast Pens", objtype = 4, tier = 4, pair = 1, map = L"Black Crag", zone = 4 },
	
	
	[5086] = {name = L"Frostbeard's Quarry", objtype = 1, tier = 4, pair = 2, map = L"Riekland", zone = 2 },
	[5087] = {name = L"Schwenderhalle Manor", objtype = 2, tier = 4, pair = 2, map = L"Riekland", zone = 2 },
	[5085] = {name = L"Reikwatch", objtype = 3, tier = 4, pair = 2, map = L"Riekland", zone = 2 },
	[5088] = {name = L"Runehammer Gunworks", objtype = 4, tier = 4, pair = 2, map = L"Riekland", zone = 2 },
	
	[5080] = {name = L"Martyr's Square", objtype = 1, tier = 4, pair = 2, map = L"Praag", zone = 3 },
	[5084] = {name = L"Manor of Ortel von Zaris", objtype = 2, tier = 4, pair = 2, map = L"Praag", zone = 3 },
	[5081] = {name = L"Kurlov's Armory", objtype = 3, tier = 4, pair = 2, map = L"Praag", zone = 3 },
	[5083] = {name = L"Russenscheller Graveyard", objtype = 4, tier = 4, pair = 2, map = L"Praag", zone = 3 },

	[5077] = {name = L"Thaugamond Massif", objtype = 1, tier = 4, pair = 2, map = L"Chaos Wastes", zone = 4 },
	[5078] = {name = L"Chokethorn Bramble", objtype = 2, tier = 4, pair = 2, map = L"Chaos Wastes", zone = 4 },
	[5076] = {name = L"The Statue of Everchosen", objtype = 3, tier = 4, pair = 2, map = L"Chaos Wastes", zone = 4 },
	[5079] = {name = L"The Shrine of Time", objtype = 4, tier = 4, pair = 2, map = L"Chaos Wastes", zone = 4 },
	
	
	[632] = {name = L"Chillwind Manor", objtype = 1, tier = 4, pair = 3, map = L"Eataine", zone = 2 },
	[635] = {name = L"Ulthorin Siege Camp", objtype = 2, tier = 4, pair = 3, map = L"Eataine", zone = 2 },
	[634] = {name = L"Sanctuary of Dreams" , objtype = 3, tier = 4, pair = 3, map = L"Eataine", zone = 2 },
	[633] = {name = L"Bel-Korhadris\x2019 Solitude", objtype = 4, tier = 4, pair = 3, map = L"Eataine", zone = 2 },
	
	[612] = {name = L"Milaith's Memory", objtype = 1, tier = 4, pair = 3, map = L"Dragonwake", zone = 3 },
	[615] = {name = L"Fireguard Spire", objtype = 2, tier = 4, pair = 3, map = L"Dragonwake", zone = 3 },
	[614] = {name = L"Pelgorath's Ember", objtype = 3, tier = 4, pair = 3, map = L"Dragonwake", zone = 3 },
	[613] = {name = L"Mournfire's Approach", objtype = 4, tier = 4, pair = 3, map = L"Dragonwake", zone = 3 },

	[636] = {name = L"Senlathain Stand", objtype = 1, tier = 4, pair = 3, map = L"Caledor", zone = 4 },
	[639] = {name = L"Druchii Barracks", objtype = 2, tier = 4, pair = 3, map = L"Caledor", zone = 4 },
	[638] = {name = L"Shrine of the Conqueror", objtype = 3, tier = 4, pair = 3, map = L"Caledor", zone = 4 },
	[637] = {name = L"Sarathanan Vale", objtype = 4, tier = 4, pair = 3, map = L"Caledor", zone = 4 }
	
	
}
