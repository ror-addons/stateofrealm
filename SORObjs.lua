SORObjs = {}

-- SystemData.Events.OBJECTIVE_OWNER_UPDATED
-- GetGameDataObjectives()

local TierID = false
local Showthis = 1
local oldSetAlertData = nil

local LockedObjTimers = {}
local ContestedObjTimers = {}

local curObjs = {}
local curKeeps = {}

local BroadcastBO = {}
local BOID = {}
local BOInfo = {}
local DeadZone = {}

local timeCount = 0
local lastUpdate = 0

-- cache the last time we called this
local gameObjs

local bars = 0


-------------- TEST -----------------

function SORObjs.Test(obj, side, counter)
	if not side then side = 1 end
	if not counter then counter = 30 end
	SORObjs.AddContestedBO(obj, side, counter)
end

function SORObjs.Test2(obj, side, counter)
	if not side then side = 1 end
	if not counter then counter = 30 end
	SORObjs.AddLockedBO(obj, side, counter)
end

function SORObjs.Clear(obj)
	SORObjs.RemoveContestedBO(obj)
	SORObjs.RemoveLockedBO(obj)
end

function SORObjs.LOT()
 d(LockedObjTimers)
end

function SORObjs.Keeps()
 d(curKeeps)
end

local function timeformat(secs)
    local trunc = math.ceil(secs)
    local mins = math.floor((1 + trunc - (trunc % 60)) / 60)
    local secs = trunc % 60
	if mins > 60 then
		local hours = math.floor((1 + mins - (mins % 60)) / 60)
		local mins = mins % 60
	    return wstring.format(L"%d:%02d:%02d", hours, mins, secs)
	end
    return wstring.format(L"%d:%02d", mins, secs)
end

function SORObjs.Hide_this()
	Showthis = 0
end

function SORObjs.Show_this()
	Showthis = 1
end

function SORObjs.GetTier()
	return TierId
end

function SORObjs.OnLoadEnd()
	SORObjs.IsTier()
	SORObjs.SelectObjIds()
	SORObjs.SelectPagerIds()
end

function SORObjs.SelectObjIds()
    curObjs = {}
    gameObjs = GetGameDataObjectives()
    for k,v in ipairs(gameObjs) do
        if v.isBattlefieldObjective and (not v.isKeep) then
            curObjs[v.id] = v
            curObjs[v.id].getid = k
            local colon = curObjs[v.id].name:find(L":")
            if colon then
                curObjs[v.id].name = curObjs[v.id].name:sub(1,colon-1)
            end
        elseif v.isBattlefieldObjective and v.isKeep then
            curKeeps[v.id] = v
            curKeeps[v.id].getid = k
		end
    end
end

function SORObjs.SelectPagerIds()
	SORPager.Reset()
	for k,v in pairs(curObjs) do
		if TierId == 4 then
			if SORS.Info[v.id] then
				if  SORS.Info[v.id].zone == SOR.GetZone() then
					SORPager.SetTable(SORS.Info[v.id].objtype, v.id , v.controllingRealm, SORS.Info[v.id].name)
				end
			end
		elseif SORS.Info[v.id] then
			SORPager.SetTable(SORS.Info[v.id].objtype, v.id , v.controllingRealm, SORS.Info[v.id].name)
		end	
	end
end

function SORObjs.IsTier()
	if not DeadZone[GameData.Player.zone] then
		local Success , PlayerTier = pcall(GetCampaignZoneData, GameData.Player.zone)
		if Success then
			TierId = PlayerTier.tierId
		else
			DeadZone[GameData.Player.zone] = true
			TierId = false
		end
	else
		TierId = false
	end
end

------------------ LOCATES CHANGED OBJECTIVES -----------------------------------------
function SORObjs.UpdateObjId(id)
    -- if we don't care about this id
    if not curObjs[id] then return end
	gameObjs = GetGameDataObjectives()
    -- fetch the new data
    local oldSide = curObjs[id].controllingRealm
    local newSide = gameObjs[curObjs[id].getid].controllingRealm
    for k,v in pairs(gameObjs[curObjs[id].getid]) do
        curObjs[id][k] = v
        if k == "name" then
            local colon = v:find(L":")
            if colon then
                curObjs[id].name = curObjs[id].name:sub(1,colon-1)
            end
        end
    end
       
    if newSide ~= oldSide then

        -- if it swapped hands, put a 3min timer for new side
        SORObjs.RemoveContestedBO(id)
        SORObjs.AddContestedBO(id, newSide)
    else
        if SORObjs.RemoveContestedBO(id) then
            SORObjs.AddLockedBO(id, newSide)
        elseif curObjs[id].open then
            SORObjs.RemoveLockedBO(id)
        end
    end
    
end


--------------------- START OBJECTIVE TRACKER -----------------------------------------
function SORObjs.Initialize()
    oldSetAlertData = AlertTextWindow.SetAlertData
    AlertTextWindow.SetAlertData = SORObjs.SetAlertDataHook
    
    RegisterEventHandler(SystemData.Events.OBJECTIVE_OWNER_UPDATED, "SORObjs.UpdateObjId")
    RegisterEventHandler(SystemData.Events.LOADING_END, "SORObjs.OnLoadEnd")
    RegisterEventHandler(SystemData.Events.RELOAD_INTERFACE, "SORObjs.OnLoadEnd")
	
    SORObjs.ShowBars(0)

		-- for bar = 1,5 do
			-- WindowSetScale("SOR.Obj" .. bar .. ".BOName", 0.65)
			-- WindowSetScale("SOR.Obj" .. bar .. ".Boon", 0.6)		
		-- end
end


----------------------- Prints the Bars ---------------------------------------------
function SORObjs.OnUpdate(timePassed)
    timeCount = timeCount + timePassed
    -- only update every 300ms
    if lastUpdate + 0.3 > timeCount then return end
    lastUpdate = timeCount
    
    local newbars = math.min(#LockedObjTimers + #ContestedObjTimers, 5)


    
	local Boon = 0
    local i = 1
    local doneindex = 0
    for k,v in ipairs(ContestedObjTimers) do
        if v.expiry <= timeCount then
            SORObjs.RemoveLockedBO(v.id)
            SORObjs.AddLockedBO(v.id, v.side, 900, SORS.Info[v.id].name)
            doneindex = k
        else
			SORPager.SetBCContested(v.id, timeformat(v.expiry - timeCount))	
			SORPager.SetTimerKnownBO(v.id, v.expiry - timeCount)
            if i <= 5 then
				if SOR_SavedSettings["TierHidden" .. SORS.Info[v.id].tier] ~= true then
					local Objwidth = ((v.expiry - timeCount) / 1.8 )
					WindowSetDimensions("SOR.Obj" .. i .. ".FBar", Objwidth, 5)
					WindowSetDimensions("SOR.Obj" .. i .. ".BBar", 100 - Objwidth, 5)				
					SOR.SetLabel("SOR.Obj" .. i .. ".Timer", timeformat(v.expiry - timeCount))
					SOR.SetLabel("SOR.Obj" .. i .. ".Boon", 
						L"Tier" .. towstring(SORS.Info[v.id].tier) .. 
						L": " .. SORS.OBJBoon[SORS.Info[v.id].objtype] )
					SOR.SetLabel("SOR.Obj" .. i .. ".BOName", SORS.Info[v.id].name)
					
					local BO = {}
					BO.id = v.id
					BO.name = SORS.Info[v.id].name
					BO.side = v.side
					BO.state = SORS.SORObjs.contested_by
					BOInfo[i] = BO
					
					
					BroadcastBO[i] = 
						L"Tier " .. SORS.Info[v.id].tier .. L" - " .. SORS.OBJBoon[SORS.Info[v.id].objtype] .. L": " 
						.. SORS.Info[v.id].name .. SORS.SORObjs.will_be_locked_down_by
						.. SORS.Side[v.side] .. SORS.SORObjs._in
						.. timeformat(v.expiry - timeCount) .. SORS.SORObjs.minutes
						

					local refnum = (SOR_SavedSettings["ColourSet"] * 100) + (SORS.Info[v.id].pair*10)

					
					if v.side == 1 then
						DynamicImageSetTexture("SOR.Obj" .. i .. ".Glow", "ObjOrd" , 32, 32)
						WindowSetTintColor("SOR.Obj" .. i .. ".FBar", 
						SORData.BarColours[refnum + 2].r , SORData.BarColours[refnum + 2].g , SORData.BarColours[refnum + 2].b )
						WindowSetTintColor("SOR.Obj" .. i .. ".BBar", 
						SORData.BarColours[refnum + 1].r , SORData.BarColours[refnum + 1].g , SORData.BarColours[refnum + 1].b )
					else
						DynamicImageSetTexture("SOR.Obj" .. i .. ".Glow", "ObjDes" , 32, 32)
						WindowSetTintColor("SOR.Obj" .. i .. ".FBar", 
						SORData.BarColours[refnum + 1].r , SORData.BarColours[refnum + 1].g , SORData.BarColours[refnum + 1].b )
						WindowSetTintColor("SOR.Obj" .. i .. ".BBar", 
						SORData.BarColours[refnum + 2].r , SORData.BarColours[refnum + 2].g , SORData.BarColours[refnum + 2].b )
					end
					i = i + 1
				end
            end
        end
    end
    for k=1,doneindex do table.remove(ContestedObjTimers, 1) end
    
    local doneindex = 0
    for k,v in ipairs(LockedObjTimers) do
        if v.expiry <= timeCount then
            doneindex = k
			SORPager.Known(v.id)
        else
			SORPager.SetBCLocked(v.id, timeformat(v.expiry - timeCount))
			SORPager.SetTimerKnownBO(v.id, v.expiry - timeCount)
            if i <= 5 then
				if SOR_SavedSettings["TierHidden" .. SORS.Info[v.id].tier] ~= true then
					local Objwidth = ((v.expiry - timeCount) / 9 )
					WindowSetDimensions("SOR.Obj" .. i .. ".FBar", Objwidth, 5)
					WindowSetDimensions("SOR.Obj" .. i .. ".BBar", 0, 5)	
					SOR.SetLabel("SOR.Obj" .. i .. ".Timer", timeformat(v.expiry - timeCount))
					SOR.SetLabel("SOR.Obj" .. i .. ".Boon", 
						L"Tier" .. towstring(SORS.Info[v.id].tier) ..
						L": " .. SORS.OBJBoon[SORS.Info[v.id].objtype])
					SOR.SetLabel("SOR.Obj" .. i .. ".BOName", SORS.Info[v.id].name)

					local BO = {}
					BO.id = v.id
					BO.name = SORS.Info[v.id].name
					BO.side = v.side
					BO.state = SORS.SORObjs.locked_by
					BOInfo[i] = BO			
					
					BroadcastBO[i] = 
						L"Tier " .. SORS.Info[v.id].tier .. L" - " .. SORS.OBJBoon[SORS.Info[v.id].objtype] .. L": " 
						.. SORS.Info[v.id].name .. SORS.SORObjs.is_locked_by
						.. SORS.Side[v.side] .. SORS.SORObjs.for_another
						.. timeformat(v.expiry - timeCount) .. SORS.SORObjs.minutes
						
						
					local refnum = (SOR_SavedSettings["ColourSet"] * 100) + (SORS.Info[v.id].pair*10)

					if v.side == 1 then
						DynamicImageSetTexture("SOR.Obj" .. i .. ".Glow", "LostOrd", 32, 32)
						WindowSetTintColor("SOR.Obj" .. i .. ".FBar", 
						SORData.BarColours[refnum + 1].r , SORData.BarColours[refnum + 1].g , SORData.BarColours[refnum + 1].b )
					else
						DynamicImageSetTexture("SOR.Obj" .. i .. ".Glow", "LostDes" , 32, 32)
						WindowSetTintColor("SOR.Obj" .. i .. ".FBar", 
						SORData.BarColours[refnum + 2].r , SORData.BarColours[refnum + 2].g , SORData.BarColours[refnum + 2].b )
					end
					i = i + 1
				end
            end
        end
    end
	SORObjs.ShowBars(i-1)
	
    for k=1,doneindex do
        if curObjs[LockedObjTimers[1].id] then
            curObjs[LockedObjTimers[1].id].open = true
        end
        table.remove(LockedObjTimers, 1)
    end
end


function SORObjs.SetAlertDataHook(vecType, vecText)
    -- posthook - we don't need to modify anything so pass it along now
    oldSetAlertData(vecType, vecText)
end

function SORObjs.RemoveContestedBO(id)
    for k,v in ipairs(ContestedObjTimers) do
        if v.id == id then
            table.remove(ContestedObjTimers, k)
            return true
        end
    end
    return false
end

function SORObjs.RemoveLockedBO(id)
    for k,v in ipairs(LockedObjTimers) do
        if v.id == id then
            table.remove(LockedObjTimers, k)
            return true
        end
    end
    return false
end

---------- ADDS A CONTESTED OBJECTIVE TO THE LIST ---(Being Captured)---
function SORObjs.AddContestedBO(id, side, timeleft)
	if SORS.Info[id] then
		local expiry = timeCount + (timeleft or 180) -- default to 3 minutes
		
		local name = SORS.Info[id].name
		
		if curObjs[id] then
			curObjs[id].open = false
		end
		
		if side == 1 or side == 2 then
			for k,v in ipairs(ContestedObjTimers) do
				if v.id == id then
					table.remove(ContestedObjTimers, k)
				end
			end
			-- insert at proper spot
			local insertspot=1
			for k,v in ipairs(ContestedObjTimers) do
				if v.expiry < expiry then
					insertspot = k+1
				else
					-- don't need to scan past anything later
					break
				end
			end
			SORPager.Contested(id, side)
			table.insert(ContestedObjTimers, insertspot, {["id"]=id, ["name"]=name, ["side"]=side, ["expiry"]=expiry})
		else
			SORObjs.Clear(id)
			SORPager.Cleared(id)
		end
	end
end

function SORObjs.IsLockedBO(id)
		for k,v in ipairs(LockedObjTimers) do
			if v.id == id then
				return v.side
			end
		end
	return false
end


----- ADDS A LOCKED OBJECTIVE TO THE LIST -(Can't be captured for 15 mins)---
function SORObjs.AddLockedBO(id, side, timeleft, name)
	if SORS.Info[id] then
		local expiry = timeCount + (timeleft or 900) -- default to 15 minutes
		
		local name = SORS.Info[id].name
		
		if (not name) and curObjs[id] then
			name = SORS.Info[id].name
		end
		
		if curObjs[id] then
			curObjs[id].open = false
		end
		
		for k,v in ipairs(LockedObjTimers) do
			if v.id == id then
				table.remove(LockedObjTimers, k)
			end
		end
		
		-- we can't find a name for this, oh well
		if not name then return end
		
		-- insert at proper spot
		local insertspot=1
		for k,v in ipairs(LockedObjTimers) do
			if v.expiry < expiry then
				insertspot = k+1
			else
				-- don't need to scan past anything later
				break
			end
		end
		SORPager.Locked(id, side)
		table.insert(LockedObjTimers, insertspot, {["id"]=id, ["name"]=name, ["side"]=side, ["expiry"]=expiry})
	end
end

----------------------- SHOWS BARS & HIDES UNUSED ONES -----------------
function SORObjs.ShowBars(num)

	WindowSetDimensions("SOR.Obj", 220,  32 * num)
    for i=1,num do
		WindowSetShowing("SOR.Obj" .. i , true)
    end
    for i=num+1,5 do
		WindowSetShowing("SOR.Obj" .. i , false)
    end

end

----------------------- Creates Tooltip -----------------
function SORObjs.ObjTip()
  local window = SystemData.ActiveWindow.name
  local parent = window:sub(1,8)
  local bar = tonumber(window:sub(8,8)) 
  if not BOInfo[bar] then return end  
  Tooltips.CreateTextOnlyTooltip( window, nil ) 
  Tooltips.SetTooltipText( 1, 1, towstring(BOInfo[bar].name))  
  Tooltips.SetTooltipText( 2, 1, towstring(SORS.OBJBoon[SORS.Info[BOInfo[bar].id].objtype]))
  Tooltips.SetTooltipText( 3, 1, towstring(SORS.Info[BOInfo[bar].id].map))
  Tooltips.SetTooltipText( 4, 1, BOInfo[bar].state .. SORS.Side[BOInfo[bar].side])
  
  if BOInfo[bar].side == 1 then
	Tooltips.SetTooltipColor( 4, 1, 57, 141, 255)
  else
	Tooltips.SetTooltipColor( 4, 1, 255, 55, 55 )
  end
  
  Tooltips.Finalize()
  if SOR_SavedSettings["TipLeft"] then
	Tooltips.AnchorTooltipManual("left" , parent , "right", -2,0)
  else
	Tooltips.AnchorTooltipManual("right" , parent , "left", 2,0)
  end
end

function SORObjs.OnLClick()
  local window = SystemData.ActiveWindow.name
  local parent = window:sub(1,8)
  local bar = tonumber(window:sub(8,8))
  if BOInfo[bar] then   
	EA_ChatWindow.InsertText(BOInfo[bar].name .. L" (" .. SORS.OBJBoon[SORS.Info[BOInfo[bar].id].objtype].. L") ")
  end
end

function SORObjs.ShowObjMenu()
	local window = SystemData.ActiveWindow.name
	local parent = window:sub(1,8)
	local bar = tonumber(window:sub(8,8))
	BroadcastBO[6] = bar
	
	EA_Window_ContextMenu.CreateContextMenu(parent, EA_Window_ContextMenu.BARMENU)
	
	if SOR_SavedSettings["CustomChat"] then
			EA_Window_ContextMenu.AddMenuItem(SORS.Chats.broadcast .. SORS.Chats._to .. SOR_SavedSettings["CustomChat"], SORObjs.BroadcastC, false, true, EA_Window_ContextMenu.BARMENU)
	end
	
	local channel = SOR.GetChannel()
	
	if channel == 2 then 
		EA_Window_ContextMenu.AddMenuItem(SORS.Chats.broadcast .. SORS.Chats._to .. SORS.Chats.warband, SORObjs.BroadcastW, false, true, EA_Window_ContextMenu.BARMENU)
	elseif channel == 1 then
		EA_Window_ContextMenu.AddMenuItem(SORS.Chats.broadcast .. SORS.Chats._to .. SORS.Chats.party, SORObjs.BroadcastP, false, true, EA_Window_ContextMenu.BARMENU)
	end
	if SOR.IsChannel(1) then
	EA_Window_ContextMenu.AddMenuItem(SORS.Chats.broadcast .. SORS.Chats._to .. SORS.Chats.region, SORObjs.Broadcast1, false, true, EA_Window_ContextMenu.BARMENU)
	end
	if SOR.IsChannel(2) then
	EA_Window_ContextMenu.AddMenuItem(SORS.Chats.broadcast .. SORS.Chats._to .. SORS.Chats.rvr, SORObjs.Broadcast2, false, true, EA_Window_ContextMenu.BARMENU)
	end
	
	EA_Window_ContextMenu.Finalize(EA_Window_ContextMenu.BARMENU)
end

function SORObjs.BroadcastC()
	SystemData.UserInput.ChatText = L"/".. SOR_SavedSettings["CustomChat"] .. L" " .. BroadcastBO[BroadcastBO[6]]
	BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)
end

function SORObjs.Broadcast1()
	SystemData.UserInput.ChatText = L"/1 " .. BroadcastBO[BroadcastBO[6]]
	BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)
end

function SORObjs.Broadcast2()
	SystemData.UserInput.ChatText = L"/2 " .. BroadcastBO[BroadcastBO[6]]
	BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)
end

function SORObjs.BroadcastW()
	SystemData.UserInput.ChatText = L"/war " .. BroadcastBO[BroadcastBO[6]]
	BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)
end

function SORObjs.BroadcastP()
	SystemData.UserInput.ChatText = L"/p " .. BroadcastBO[BroadcastBO[6]]
	BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)
end


function SORObjs.GrowUp()
 for i = 2,5  do
	WindowClearAnchors("SOR.Obj" .. i)
	WindowAddAnchor("SOR.Obj" .. i, "topleft", "SOR.Obj" .. i - 1 , "bottomleft", 0, 0)
 end
	WindowClearAnchors("SOR.Obj1")
	WindowAddAnchor("SOR.Obj1", "bottomleft", "SOR.Obj", "bottomleft", 0, 0)
end

function SORObjs.GrowDown()
 for i = 2,5,1 do
	WindowClearAnchors("SOR.Obj" .. i)
	WindowAddAnchor("SOR.Obj" .. i, "bottomleft", "SOR.Obj" .. i - 1 , "topleft", 0, 0)
 end
	WindowClearAnchors("SOR.Obj1")
	WindowAddAnchor("SOR.Obj1", "topleft", "SOR.Obj", "topleft", 0, 0)
end
