SORRealms = {}

local ZONE_NUMBERS = {[1]=10, [2]=9, [3]=5, [4]=3, [5]=4}

local toolTipText = {}
local oldVPorder = {}
local oldVPdestr = {}
local Broadcast = ""
local Info = ""
local CitySiege = false

local function timeformat(secs)
    local trunc = math.ceil(secs)
    local mins = math.floor((1 + trunc - (trunc % 60)) / 60)
    local secs = trunc % 60
	if mins > 60 then
		local hours = math.floor((1 + mins - (mins % 60)) / 60)
		local mins = mins % 60
	    return wstring.format(L"%d:%02d:%02d", hours, mins, secs)
	end
    return wstring.format(L"%d:%02d", mins, secs)
end

function SORRealms.AlphaUpdate()
	for i = 0, 3 do
		WindowSetAlpha("SOR.Realm.alpha" .. i .. ".Alpha" , SOR_SavedSettings["BackgroundAlpha"])
		if SOR_SavedSettings["BackgroundAlpha"] * 2 > 1 then
			WindowSetAlpha("SOR.Realm.alpha" .. i .. ".Frame" , 1)	
		else
			WindowSetAlpha("SOR.Realm.alpha" .. i .. ".Frame" , SOR_SavedSettings["BackgroundAlpha"] * 2)
		end
	end
end

-- Function that makes Keep Icon & Tool Tip --
function SORRealms.DisplayKeepIcon(window, keepId)
  local keepData = GetKeepData(keepId)

  if(keepData == nil) then
    return
  end
  
  toolTipText[window] = keepData
  
  local keepSlice = TrackerUtils.GetKeepSliceForOwner(keepData.realmOwner)

  if(keepData.attackStatus > 1) then
      DynamicImageSetTextureSlice(window, keepSlice .. "-UnderAttack")
  else
      DynamicImageSetTextureSlice(window, keepSlice)
  end
end


---------------- CHANGES BAR DEPENDING ON ZONE ---------------------------
--------------------- Also adds Flames -----------------------------------

function SORRealms.UpdateCampaignZones(window, pairing, contestedZone)

	-- Displays Flames if fortress is under attack --
	if contestedZone == 1 then
		WindowSetShowing(window .. ".Zone.Dot1.Icon", true)
		WindowSetShowing(window .. ".Zone.Dot5.Icon", false)
	elseif contestedZone == 5 then
		WindowSetShowing(window .. ".Zone.Dot1.Icon", false)
		WindowSetShowing(window .. ".Zone.Dot5.Icon", true)	
	else
		WindowSetShowing(window .. ".Zone.Dot1.Icon", false)
		WindowSetShowing(window .. ".Zone.Dot5.Icon", false)	
	end

	if contestedZone ~= 0 then
		for i=1,4 do
			WindowSetShowing(window .. ".Zone.ZBar" .. i , true)
			local refnum = 2
			if contestedZone > i then
				refnum = 1
			end
				refnum = (SOR_SavedSettings["ColourSet"] * 100) + (pairing*10) + refnum
				WindowSetTintColor(window .. ".Zone.ZBar" .. i, 
				SORData.BarColours[refnum].r , SORData.BarColours[refnum].g , SORData.BarColours[refnum].b )
		end
	else
		for i=1,4 do
			WindowSetShowing(window .. ".Zone.ZBar" .. i , false)
		end
	end
	
end

function SORRealms.Setup()
	for i = 1, 3 do
		for j = 1, 3 do
		    toolTipText["SOR.Realm" .. i .. ".Low." .. j .. "Label"] = L"Tier " .. towstring(j).. L": " .. SORS.Pairings[i] .. L" "
			LabelSetText("SOR.Realm" .. i .. ".Low." .. j .. "Label", towstring(j))
			LabelSetTextColor("SOR.Realm" .. i .. ".Low." .. j .. "Label", 
				SOR_SavedSettings["TierR"],
				SOR_SavedSettings["TierG"], 
				SOR_SavedSettings["TierB"])
		end
		toolTipText["SOR.Realm" .. i .. ".High.4Label"] = L"Tier 4: " .. SORS.Pairings[i]
		LabelSetText("SOR.Realm" .. i .. ".High.4Label", L"4")
		LabelSetTextColor("SOR.Realm" .. i .. ".High.4Label", 
			SOR_SavedSettings["TierR"],
			SOR_SavedSettings["TierG"], 
			SOR_SavedSettings["TierB"])
		SOR.SetLabel("SOR.Realm" .. i .. ".Fortress", SORS.SORRealms.under_siege)	
		DynamicImageSetTexture("SOR.Realm" .. i .. ".Zone.BackImage", SOR_SavedSettings["GraphicSet"] .. "IconRealm" .. i , 0 , 0)
		DynamicImageSetTexture("SOR.Realm" .. i .. ".LowImage", SOR_SavedSettings["GraphicSet"] .. "IconRealm" .. i .. "Low", 0 , 0)
		-- WindowSetScale("SOR.Realm" .. i .. ".High.OVPC", 0.8)
		-- WindowSetScale("SOR.Realm" .. i .. ".High.DVPC", 0.8)
	end
end

---------------- UPDATES THE KEEP INFORMATION ------------------------
--------------------- and Draws Keeps --------------------------------

function SORRealms.UpdateKeepStatus(window, pairing, zone)
    -- Tier 4 keeps are ids 1-10 (orc), 11-20 (human), and 21-30 (elf)
    local keepId = (pairing - 1) * 10 + (zone - 2) * 2 + 5
    local lowkeepId = (pairing - 1) * 10 + 1

    SORRealms.DisplayKeepIcon(window .. ".Low.Keep2a.Icon", lowkeepId)
    SORRealms.DisplayKeepIcon(window .. ".Low.Keep2b.Icon", lowkeepId + 1)
    SORRealms.DisplayKeepIcon(window .. ".Low.Keep3a.Icon", lowkeepId + 2)
    SORRealms.DisplayKeepIcon(window .. ".Low.Keep3b.Icon", lowkeepId + 3)
	
	if (zone > 4 or zone < 2) then
		WindowSetShowing(window .. ".High.Keep4a.Icon", false)
		WindowSetShowing(window .. ".High.Keep4b.Icon", false)
    else
	    WindowSetShowing(window .. ".High.Keep4a.Icon", true)
		WindowSetShowing(window .. ".High.Keep4b.Icon", true)
		SORRealms.DisplayKeepIcon(window .. ".High.Keep4a.Icon", keepId)
		SORRealms.DisplayKeepIcon(window .. ".High.Keep4b.Icon", keepId + 1)	
    end
end

---------------- UPDATES TIER CONTROL PERCENTAGES --------------------
-------------------- and sends them to Labels ------------------------

function SORRealms.UpdatePercentages(window, pairing)
	for i = 1, 3 do
	local Tierpairing = (pairing - 1) * 100 + 5 + i
	local zoneData = GetCampaignZoneData(Tierpairing)
	local orderPoints = string.format("%.0f", zoneData.controlPoints[ GameData.Realm.ORDER ] / .667)
	local destructionPoints =  string.format("%.0f", zoneData.controlPoints[ GameData.Realm.DESTRUCTION ] / .667)

	SOR.SetLabel(window .. ".Low." .. i .. "VP", towstring(orderPoints .. "/" .. destructionPoints))
	local VP = {}
		VP.pairing = pairing
		VP.tier = i
		VP.order = orderPoints
		VP.destr = destructionPoints
	toolTipText["SOR.Realm" .. pairing .. ".Low." .. i .. "VP"] = VP
   end
end


---------------- UPDATES ALL THE LABELS FOR REALMS --------------------
----------------including the hidden fortress siege -------------------

function SORRealms.UpdateLabels (window, pairData)
	SOR.SetLabel(window .. ".Battlezone", towstring(GetZoneName(pairData.contestedZone)))
end

function SORRealms.UpdateCity(change)
	if GameData.CityScenarioData.timeLeft > 0 then
		SOR.SetLabel("SOR.Realm0.Fortress", towstring(timeformat(GameData.CityScenarioData.timeLeft)))
		
	local pairData = GetCampaignPairingData( 1 )
	local zone = SOR.ConvertZoneNumber(pairData.contestedZone, 1)
	local zoneData = GetCampaignZoneData(pairData.contestedZone)
	local orderPoints = string.format("%.0f", zoneData.controlPoints[ GameData.Realm.ORDER ] / .667)
	local destructionPoints =  string.format("%.0f", zoneData.controlPoints[ GameData.Realm.DESTRUCTION ] / .667)
					SOR.SetLabel("SOR.Realm0.High.OVP", towstring(orderPoints .. "%"))
					SOR.SetLabel("SOR.Realm0.High.DVP", towstring(destructionPoints .. "%"))

	if change ~= 0 then
		SORRealms.Swing("SOR.Realm0", 1, 1, orderPoints, destructionPoints)
	end					
					
		local ZoneData = GetCampaignZoneData( 162 )
		if ZoneData.controllingRealm == 0 then
			SOR.SetLabel("SOR.Realm0.Battlezone", SORS.SORRealms.altdorf)
		elseif ZoneData.controllingRealm == 2 then
			SOR.SetLabel("SOR.Realm0.Battlezone", SORS.SORRealms.altdorf_taken)
		else
			ZoneData = GetCampaignZoneData( 161 )
			if ZoneData.controllingRealm == 0 then
				SOR.SetLabel("SOR.Realm0.Battlezone", SORS.SORRealms.inevitable_city)	
			elseif ZoneData.controllingRealm == 1 then
				SOR.SetLabel("SOR.Realm0.Battlezone", SORS.SORRealms.inevitable_city_taken)	
			else
				SOR.SetLabel("SOR.Realm0.Battlezone", SORS.SORRealms.safe)		
			end
		end
	end
end


----------------------- MAIN FUNCTION --------------------------------
----- Calls Other Draw Functions Depending on Realm Status -----------
function SORRealms.UpdateZoneControl(window, pairing, change)
	local pairData = GetCampaignPairingData( pairing )
	local zone = SOR.ConvertZoneNumber(pairData.contestedZone, pairing)
	local orderPoints = 0
	local destructionPoints = 0
	if pairData.contestedZone ~= 0 then
		local zoneData = GetCampaignZoneData(pairData.contestedZone)
		orderPoints = string.format("%.0f", zoneData.controlPoints[ GameData.Realm.ORDER ] / .667)
		destructionPoints =  string.format("%.0f", zoneData.controlPoints[ GameData.Realm.DESTRUCTION ] / .667)
	end	

			if pairData.isLocked then
				SOR.SetLabel("SOR.Realm" .. pairing .. ".Fortress", SORS.SORRealms.locked_down)
				toolTipText["SOR.Realm" .. pairing .. ".Fortress"] = SORS.SORRealms.the .. SORS.Pairings[pairing] .. SORS.SORRealms.is_locked
					WindowSetShowing(window .. ".High", false)
					WindowSetShowing(window .. ".Fortress", true)
					WindowSetShowing(window .. ".Battlezone", false)
					if(pairData.controllingRealm == 1) then
						zone = 5
					elseif(pairData.controllingRealm == 2) then
						zone = 1
					else
						zone = 0
					end
					SORRealms.UpdateCampaignZones(window, pairing, zone)  

			elseif(pairData.controllingRealm ~= 0) then
				SOR.SetLabel("SOR.Realm" .. pairing .. ".Battlezone", SORS.SORRealms.realm_taken)
				SOR.SetLabel("SOR.Realm" .. pairing .. ".Fortress", timeformat(pairData.captureTimeRemaining))
				toolTipText["SOR.Realm" .. pairing .. ".Fortress"] = SORS.SORRealms.the .. SORS.Pairings[pairing]
											.. SORS.SORRealms.has_been_taken_by .. SORS.Side[pairData.controllingRealm]
					WindowSetShowing(window .. ".High", false)
					WindowSetShowing(window .. ".Fortress", true)
					WindowSetShowing(window .. ".Battlezone", true)
				
				if(pairData.controllingRealm == 1) then
					zone = 5
				else
					zone = 1
				end
				SORRealms.UpdateCampaignZones(window, pairing, zone)  

			else--  NOT if(pairData.controllingRealm ~= 0)
				SOR.SetLabel("SOR.Realm" .. pairing .. ".Fortress", SORS.SORRealms.under_siege)
				WindowSetShowing(window .. ".Battlezone", true)
			------------- REALM NOT LOCKED DOWN -----------------

				SORRealms.UpdateCampaignZones(window, pairing, zone)  

				if(zone == 1 or zone == 5) then
				---------- ATTACKING FORTRESS --------------
						WindowSetShowing(window .. ".High", false)
						WindowSetShowing(window .. ".Fortress", true)
						local Fside = 1
						if zone == 1 then Fside = 2 end
						toolTipText["SOR.Realm" .. pairing .. ".Fortress"] = GetZoneName(pairData.contestedZone) .. SORS.SORRealms.is_under_attack_by .. SORS.Side[Fside]
				else 
				
				-------- NOT ATTACKING FORTRESS ------------

					WindowSetShowing(window .. ".High", true)
					WindowSetShowing(window .. ".Fortress", false)			
					SOR.SetLabel(window .. ".High.OVP", towstring(orderPoints .. "%"))
					SOR.SetLabel(window .. ".High.DVP", towstring(destructionPoints .. "%"))

				end
				SORRealms.UpdateLabels (window, pairData)
			end
	

	-- City Siege --
	if GameData.CityScenarioData.timeLeft > 0 then	
		if CitySiege == false then
			CitySiege = true
			SOR.SetupWindows()
		end	
	else
		if CitySiege then
			SORObjs.SelectObjIds()
			CitySiege = false
			SOR.SetupWindows()
		end
	end
	
	-- All -- 
	
	if SOR_SavedSettings["RealmMode"] == "Low" then
		WindowSetShowing(window .. ".High", false)
		WindowSetShowing(window .. ".Fortress", false)
		WindowSetShowing(window .. ".Battlezone", false)	
	end
	
	if change ~= 0 then
		SORRealms.Swing(window, pairing, 1, orderPoints, destructionPoints)
	end	
	SORRealms.UpdateKeepStatus(window, pairing, zone)
	SORRealms.UpdatePercentages(window, pairing)
	
end

function SORRealms.City()
	CitySiege = true
end 

function SORRealms.Swing(window, pairing, update, ordVP, desVP)
		local pairData = GetCampaignPairingData( pairing )
		ShiftPosition = SOR.GetShiftPosition()
		local VPslot = pairing * 1000 + ShiftPosition 
		local oldVPslot = VPslot - SOR_SavedSettings["SwingShift"] + 370
		if ShiftPosition  > SOR_SavedSettings["SwingShift"] then
			oldVPslot = VPslot - SOR_SavedSettings["SwingShift"]
		end
		if update == 1 then
			oldVPorder[VPslot] = ordVP
			oldVPdestr[VPslot] = desVP
		end
			if not oldVPorder[VPslot] then oldVPorder[VPslot] = 0 end 
			if not oldVPdestr[VPslot] then oldVPdestr[VPslot] = 0 end 
			if not oldVPorder[oldVPslot] then oldVPorder[oldVPslot] = 0 end 
			if not oldVPdestr[oldVPslot] then oldVPdestr[oldVPslot] = 0 end 
		local changeOVP = oldVPorder[VPslot] - oldVPorder[oldVPslot]
		local changeDVP = oldVPdestr[VPslot] - oldVPdestr[oldVPslot]
		local VPO = {}
			VPO.swing = changeOVP
			VPO.current = oldVPorder[VPslot]
			VPO.side = 1
			VPO.zonename = towstring(GetZoneName(pairData.contestedZone))
			VPO.pairing = pairing
		toolTipText[window .. ".High.OVP"] = VPO
		local VPD = {}
			VPD.swing = changeDVP
			VPD.current = oldVPdestr[VPslot]
			VPD.side = 2
			VPD.zonename = towstring(GetZoneName(pairData.contestedZone))
			VPD.pairing = pairing
		toolTipText[window .. ".High.DVP"] = VPD	
		
		if changeOVP == 0 then
			LabelSetText(window .. ".High.OVPC", towstring(""))
		else 					
			LabelSetText(window .. ".High.OVPC", towstring(changeOVP .. "%"))
			LabelSetTextColor(window .. ".High.OVPC", 200, 200, 200)
			if changeOVP > 9 then
				LabelSetTextColor(window .. ".High.OVPC", 155, 170, 255)
			elseif changeOVP > 4 then
				LabelSetTextColor(window .. ".High.OVPC", 130, 137, 179)
			end
		end
		
		if changeDVP == 0 then
			LabelSetText(window .. ".High.DVPC", towstring(""))
		else
			LabelSetText(window .. ".High.DVPC", towstring(changeDVP .. "%"))
			LabelSetTextColor(window .. ".High.DVPC", 200, 200, 200)
			if changeDVP > 9 then
				LabelSetTextColor(window .. ".High.DVPC", 255, 55, 55)
			elseif changeDVP > 4 then
				LabelSetTextColor(window .. ".High.DVPC", 201, 101, 101)
			end
		end
end

function SORRealms.MakeZones()
  for i = 1, 3 do
    for j = 1, 5 do
      local zoneNumber = ((i - 1) * 100 ) + ZONE_NUMBERS[j]
      toolTipText["SOR.Realm" .. i .. ".Zone.Dot" .. j .. ".Hover"] =  zoneNumber
    end
  end 
end

--------------------------------- Sends Data from City -----------------------------------------

function SORRealms.ShowCityMenu()
	local window = SystemData.ActiveWindow.name
		
		EA_Window_ContextMenu.CreateContextMenu(window, EA_Window_ContextMenu.BARMENU)
		
		local channel = SOR.GetChannel()
	
 	if SOR_SavedSettings["CustomChat"] then
			EA_Window_ContextMenu.AddMenuItem(SORS.Chats.send_info .. SORSChats._to .. SOR_SavedSettings["CustomChat"],
			SORRealms.BroadcastCityC, false, true, EA_Window_ContextMenu.BARMENU)

	end	
	
		if channel == 2 then 
			EA_Window_ContextMenu.AddMenuItem(SORS.Chats.send_info .. SORSChats._to .. SORS.Chats.warband, SORRealms.BroadcastCityW, false, true, EA_Window_ContextMenu.BARMENU)
		elseif channel == 1 then
			EA_Window_ContextMenu.AddMenuItem(SORS.Chats.send_info .. SORSChats._to .. SORS.Chats.party, SORRealms.BroadcastCityP, false, true, EA_Window_ContextMenu.BARMENU)
		end	
		if SOR.IsChannel(1) then
		EA_Window_ContextMenu.AddMenuItem(SORS.Chats.send_info .. SORSChats._to .. SORS.Chats.region, SORRealms.BroadcastCity1, false, true, EA_Window_ContextMenu.BARMENU)
		end
		if SOR.IsChannel(2) then
		EA_Window_ContextMenu.AddMenuItem(SORS.Chats.send_info .. SORSChats._to .. SORS.Chats.rvr, SORRealms.BroadcastCity2, false, true, EA_Window_ContextMenu.BARMENU)
		end
		
		EA_Window_ContextMenu.Finalize(EA_Window_ContextMenu.BARMENU)
	
end

--------------------------------- Sends Data from Fortress -----------------------------------------

function SORRealms.ShowFortressMenu()
	local window = SystemData.ActiveWindow.name
	if toolTipText[window] then	

		Broadcast = toolTipText[window]
		
		EA_Window_ContextMenu.CreateContextMenu(window, EA_Window_ContextMenu.BARMENU)
		
		local channel = SOR.GetChannel()

 	if SOR_SavedSettings["CustomChat"] then
			EA_Window_ContextMenu.AddMenuItem(SORS.Chats.send_info .. SORSChats._to .. SOR_SavedSettings["CustomChat"],
			SORRealms.BroadcastW, false, true, EA_Window_ContextMenu.BARMENU)

	end	
		
		if channel == 2 then 
			EA_Window_ContextMenu.AddMenuItem(SORS.Chats.send_info .. SORSChats._to .. SORS.Chats.warband, SORRealms.BroadcastW, false, true, EA_Window_ContextMenu.BARMENU)
		elseif channel == 1 then
			EA_Window_ContextMenu.AddMenuItem(SORS.Chats.send_info .. SORSChats._to .. SORS.Chats.party, SORRealms.BroadcastP, false, true, EA_Window_ContextMenu.BARMENU)
		end	
		if SOR.IsChannel(1) then
		EA_Window_ContextMenu.AddMenuItem(SORS.Chats.send_info .. SORSChats._to .. SORS.Chats.region, SORRealms.Broadcast1, false, true, EA_Window_ContextMenu.BARMENU)
		end
		if SOR.IsChannel(2) then
		EA_Window_ContextMenu.AddMenuItem(SORS.Chats.send_info .. SORSChats._to .. SORS.Chats.rvr, SORRealms.Broadcast1, false, true, EA_Window_ContextMenu.BARMENU)	
		end
		
		EA_Window_ContextMenu.Finalize(EA_Window_ContextMenu.BARMENU)
	end
end

--------------------------------- Sends Data from VPs TIER4 ----------------------------------------

function SORRealms.OnMouseOverVP()
	local window = SystemData.ActiveWindow.name
	local VP = toolTipText[window]
	local parent = window:sub(1,10)

	Tooltips.CreateTextOnlyTooltip( SystemData.ActiveWindow.name, nil ) 
	Tooltips.SetTooltipText( 1, 1, SORS.Side[VP.side] .. SORS.SORRealms.has .. towstring(VP.current) .. SORS.SORRealms.percent_control_of_zone)
	  if VP.side == 1 then
		Tooltips.SetTooltipColor( 1, 1, 57, 141, 255)
	  else
		Tooltips.SetTooltipColor( 1, 1, 255, 55, 55 )
	  end
	  
	  	Tooltips.SetTooltipColor( 2, 1, 190, 190, 190 )
		Tooltips.SetTooltipColor( 3, 1, 190, 190, 190 )
	  
	  if VP.swing > 0 then
		Tooltips.SetTooltipText( 2, 1, SORS.SORRealms.with_a .. towstring(VP.swing) .. SORS.SORRealms.percent_swing_to .. SORS.Side[VP.side])	
	  elseif VP.swing < 0 then
		Tooltips.SetTooltipText( 2, 1, SORS.SORRealms.with_a .. towstring(VP.swing) .. SORS.SORRealms.percent_swing_from .. SORS.Side[VP.side])
	  else
	  	Tooltips.SetTooltipText( 2, 1, SORS.SORRealms.no_change)
		Tooltips.SetTooltipColor( 2, 1, 100, 100, 100 )
		Tooltips.SetTooltipColor( 3, 1, 100, 100, 100 )
	  end
		Tooltips.SetTooltipText( 3, 1, SORS.SORRealms.in_the_last .. timeformat(SOR_SavedSettings["SwingShift"] * 10) .. SORS.SORRealms.minutes)

		
	Tooltips.Finalize()
	  if SOR_SavedSettings["TipLeft"] then
		Tooltips.AnchorTooltipManual("left" , parent , "right", -2,0)
	  else
		Tooltips.AnchorTooltipManual("right" , parent , "left", 2,0)
	  end
end

function SORRealms.ShowVPMenu()
	local window = SystemData.ActiveWindow.name
	local VP = toolTipText[window]	

	  if VP.swing > 0 then
	Broadcast = SORS.Side[VP.side] .. SORS.SORRealms.has .. towstring(VP.current) .. SORS.SORRealms.percent_control_of
				.. VP.zonename .. SORS.SORRealms.with_a .. towstring(VP.swing) .. SORS.SORRealms.percent_swing_to .. SORS.Side[VP.side]
				.. SORS.SORRealms.in_the_last .. timeformat(SOR_SavedSettings["SwingShift"] * 10) .. SORS.SORRealms.minutes
	  elseif VP.swing < 0 then
	Broadcast = SORS.Side[VP.side] .. SORS.SORRealms.has .. towstring(VP.current) .. SORS.SORRealms.percent_control_of
				.. VP.zonename .. SORS.SORRealms.with_a .. towstring(VP.swing) .. SORS.SORRealms.percent_swing_from .. SORS.Side[VP.side]
				.. SORS.SORRealms.in_the_last .. timeformat(SOR_SavedSettings["SwingShift"] * 10) .. SORS.SORRealms.minutes
    else
	Broadcast = SORS.Side[VP.side] .. SORS.SORRealms.has .. towstring(VP.current) .. SORS.SORRealms.percent_control_of
				.. VP.zonename .. L" - " .. SORS.SORRealms.no_change .. SORS.SORRealms.in_the_last .. timeformat(SOR_SavedSettings["SwingShift"] * 10) .. SORS.SORRealms.minutes
    end
	
	EA_Window_ContextMenu.CreateContextMenu(window, EA_Window_ContextMenu.BARMENU)
	
	local channel = SOR.GetChannel()
 
 	if SOR_SavedSettings["CustomChat"] then
		EA_Window_ContextMenu.AddMenuItem(SORS.Chats.send_info .. SORS.Chats._to .. SOR_SavedSettings["CustomChat"],
		SORRealms.BroadcastC, false, true, EA_Window_ContextMenu.BARMENU)

	end	
 
	if channel == 2 then 
		EA_Window_ContextMenu.AddMenuItem(SORS.Chats.send_info .. SORS.Chats._to .. SORS.Chats.warband, SORRealms.BroadcastW, false, true, EA_Window_ContextMenu.BARMENU)
	elseif channel == 1 then
		EA_Window_ContextMenu.AddMenuItem(SORS.Chats.send_info .. SORS.Chats._to .. SORS.Chats.party, SORRealms.BroadcastP, false, true, EA_Window_ContextMenu.BARMENU)
	end	
	if SOR.IsChannel(1) then
	EA_Window_ContextMenu.AddMenuItem(SORS.Chats.send_info .. SORS.Chats._to .. SORS.Chats.region, SORRealms.Broadcast1, false, true, EA_Window_ContextMenu.BARMENU)
	end
	if SOR.IsChannel(2) then
	EA_Window_ContextMenu.AddMenuItem(SORS.Chats.send_info .. SORS.Chats._to .. SORS.Chats.rvr, SORRealms.Broadcast2, false, true, EA_Window_ContextMenu.BARMENU)
	end
	
	EA_Window_ContextMenu.Finalize(EA_Window_ContextMenu.BARMENU)
end

--------------------------------- Sends Data from VPs TIERs 123 ----------------------------------------

function SORRealms.OnLClickTier()
	local window = SystemData.ActiveWindow.name
	EA_ChatWindow.InsertText(toolTipText[window])
end

function SORRealms.OnMouseOverLowVP()
	local window = SystemData.ActiveWindow.name
	local VP = toolTipText[window]
	Tooltips.CreateTextOnlyTooltip( SystemData.ActiveWindow.name, nil ) 
	Tooltips.SetTooltipText( 1, 1, L"Tier " .. towstring(VP.tier) .. L": " .. SORS.Pairings[VP.pairing])
	Tooltips.SetTooltipText( 2, 1, SORS.Side[1] .. SORS.SORRealms.has .. towstring(VP.order) .. SORS.SORRealms.percent_control)
	Tooltips.SetTooltipText( 3, 1, SORS.Side[2] .. SORS.SORRealms.has .. towstring(VP.destr) .. SORS.SORRealms.percent_control)	
		Tooltips.SetTooltipColor( 2, 1, 57, 141, 255)
		Tooltips.SetTooltipColor( 3, 1, 255, 55, 55 )
		
	Tooltips.Finalize()
	  if SOR_SavedSettings["TipLeft"] then
		Tooltips.AnchorTooltipManual("left" , "SOR.Realm" .. VP.pairing , "right", -2,0)
	  else
		Tooltips.AnchorTooltipManual("right" , "SOR.Realm" .. VP.pairing , "left", 2,0)
	  end
end

function SORRealms.ShowLowVPMenu()
	local window = SystemData.ActiveWindow.name
	local VP = toolTipText[window]	

	Broadcast = L"Tier " .. towstring(VP.tier) .. L": " .. SORS.Pairings[VP.pairing]
	.. L". " .. SORS.Side[1] .. SORS.SORRealms.has .. towstring(VP.order) .. SORS.SORRealms.percent_control .. L". " .. SORS.Side[2] .. SORS.SORRealms.has .. towstring(VP.destr) .. SORS.SORRealms.percent_control
	
	EA_Window_ContextMenu.CreateContextMenu(window, EA_Window_ContextMenu.BARMENU)
	
	local channel = SOR.GetChannel()
 
	if SOR_SavedSettings["CustomChat"] then
		EA_Window_ContextMenu.AddMenuItem(SORS.Chats.send_info .. SORS.Chats._to .. SOR_SavedSettings["CustomChat"],
		SORRealms.BroadcastC, false, true, EA_Window_ContextMenu.BARMENU)
	end	
 
	if channel == 2 then 
		EA_Window_ContextMenu.AddMenuItem(SORS.Chats.send_info .. SORS.Chats._to .. SORS.Chats.warband, SORRealms.BroadcastW, false, true, EA_Window_ContextMenu.BARMENU)
	elseif channel == 1 then
		EA_Window_ContextMenu.AddMenuItem(SORS.Chats.send_info .. SORS.Chats._to .. SORS.Chats.party, SORRealms.BroadcastP, false, true, EA_Window_ContextMenu.BARMENU)
	end	
	if SOR.IsChannel(1) then
	EA_Window_ContextMenu.AddMenuItem(SORS.Chats.send_info .. SORS.Chats._to .. SORS.Chats.region, SORRealms.Broadcast1, false, true, EA_Window_ContextMenu.BARMENU)
	end
	if SOR.IsChannel(2) then
	EA_Window_ContextMenu.AddMenuItem(SORS.Chats.send_info .. SORS.Chats._to .. SORS.Chats.rvr, SORRealms.Broadcast2, false, true, EA_Window_ContextMenu.BARMENU)
	end
	EA_Window_ContextMenu.Finalize(EA_Window_ContextMenu.BARMENU)
end

--------------------------------- Creates Tooltip for Zones ----------------------------------------

function  SORRealms.OnMouseOverHover()
  local window = SystemData.ActiveWindow.name
  local ZoneId = toolTipText[window]
  
  if ZoneId == nil or window == nil then
    return
  end  

  local ZoneName = GetZoneName(ZoneId)
  local pairing = SOR.ConvertPairing(ZoneId)
  local pairData = GetCampaignPairingData(pairing)
  
  local ZoneContested = SOR.ConvertZoneNumber(pairData.contestedZone, pairing)
  
  if(pairData.controllingRealm == 1) then
	ZoneContested = 5
  elseif(pairData.controllingRealm == 2) then
	ZoneContested = 1
  end

  local Zone = SOR.ConvertZoneNumber(ZoneId, pairing)
  Tooltips.CreateTextOnlyTooltip( SystemData.ActiveWindow.name, nil ) 
  Tooltips.SetTooltipText( 1, 1, towstring(ZoneName))
  if pairData.isLocked then
  	  Tooltips.SetTooltipText( 2, 1, SORS.SORRealms.locked_down)
	  Tooltips.SetTooltipColor( 2, 1, 190, 190, 190)
  elseif Zone > ZoneContested then
	  Tooltips.SetTooltipText( 2, 1, SORS.SORRealms.locked_by .. SORS.Side[2])
	  Tooltips.SetTooltipColor( 2, 1, 255, 55, 55 )
  elseif Zone < ZoneContested then
	  Tooltips.SetTooltipText( 2, 1, SORS.SORRealms.locked_by .. SORS.Side[1])
	  Tooltips.SetTooltipColor( 2, 1, 57, 141, 255)
  else
	  Tooltips.SetTooltipText( 2, 1, SORS.SORRealms.contested)
	  Tooltips.SetTooltipColor( 2, 1, 255, 255, 255)
  end
  
  Tooltips.Finalize()
  if SOR_SavedSettings["TipLeft"] then
	Tooltips.AnchorTooltipManual("left" , "SOR.Realm" .. pairing , "right", -2,0)
  else
	Tooltips.AnchorTooltipManual("right" , "SOR.Realm" .. pairing , "left", 2,0)
  end
end

function SORRealms.OnLClickZone()
  local window = SystemData.ActiveWindow.name
  local ZoneId = toolTipText[window]
	EA_ChatWindow.InsertText(GetZoneName(ZoneId) .. L" ")
end

function SORRealms.OnLClickBZ()
	local window = SystemData.ActiveWindow.name
	EA_ChatWindow.InsertText(LabelGetText(window) .. L" ")
end


--------------------------------- Creates Tooltip for Keeps ----------------------------------------

function SORRealms.OnMouseOverKeep()
  local window = SystemData.ActiveWindow.name
  if  SORData.KeeptoLabel[window].level == "low" then
	SORPager.LowBOsOn()
  else
	SORPager.HighBOsOn()
  end
  local keepData = toolTipText[window]
  if(keepData == nil) then
    return
  end

  local pairing = 1
	if keepData.id > 20 then
	 pairing = 3	
	elseif  keepData.id > 10 then
	 pairing = 2
	end
 
  Tooltips.CreateTextOnlyTooltip( window, nil ) 
  
  Tooltips.SetTooltipText( 1, 1, SORS.KeepNames[keepData.id])
  Tooltips.SetTooltipText( 2, 1, SORS.KeepMap[keepData.id] )
  if keepData.guildOwner:len() > 0  then
	Tooltips.SetTooltipText( 3, 1, keepData.guildOwner)
	Tooltips.SetTooltipColor( 3, 1, 190, 190, 190 )
  else
	Tooltips.SetTooltipText( 3, 1, SORS.SORRealms.not_owned)
	Tooltips.SetTooltipColor( 3, 1, 100, 100, 100 )
  end
  
  
  Tooltips.SetTooltipText( 4, 1, SORS.SORRealms.controlled_by .. SORS.Side[keepData.realmOwner])
  if keepData.realmOwner == 1 then
  	Tooltips.SetTooltipColor( 4, 1, 57, 141, 255)
  else
	Tooltips.SetTooltipColor( 4, 1, 255, 55, 55 )
  end
  Tooltips.SetTooltipText( 5, 1, SORS.KeepAttackStatus[keepData.attackStatus])
  if keepData.attackStatus == 1 then
   Tooltips.SetTooltipColor( 5, 1, 100, 160, 100 )
  else
   Tooltips.SetTooltipColor( 5, 1, 255, 255, 255 )    
  end



  Tooltips.Finalize()
  if SOR_SavedSettings["TipLeft"] then
	Tooltips.AnchorTooltipManual("topleft" , "SOR.Realm" .. pairing , "topright", -2,0)
  else
	Tooltips.AnchorTooltipManual("topright" , "SOR.Realm" .. pairing , "topleft", 2,0)
  end
end

function SORRealms.OnLClickKeep()
  local window = SystemData.ActiveWindow.name
  local keepData = toolTipText[window]
  if(keepData == nil) then return end
	EA_ChatWindow.InsertText(SORS.KeepNames[keepData.id] .. L" (" .. SORS.KeepMap[keepData.id] .. L") ")
end

function SORRealms.ShowKeepMenu()
	local window = SystemData.ActiveWindow.name
	local keepData = toolTipText[window]	

	EA_Window_ContextMenu.CreateContextMenu(window, EA_Window_ContextMenu.BARMENU)
	
	local channel = SOR.GetChannel()
 	
	if GameData.Player.realm ~= keepData.realmOwner then
		Broadcast = SORS.SORRealms.heading_to .. SORS.KeepMap[keepData.id] .. SORS.SORRealms.to_take ..  SORS.KeepNames[keepData.id]
		.. SORS.SORRealms.from .. SORS.Opposition[GameData.Player.realm]
	
		if SOR_SavedSettings["CustomChat"] then
				EA_Window_ContextMenu.AddMenuItem(SORS.Chats.declare_attack .. SORS.Chats._to .. SOR_SavedSettings["CustomChat"],
				SORRealms.BroadcastC, false, true, EA_Window_ContextMenu.BARMENU)
		end	

			if channel == 2 then 
				EA_Window_ContextMenu.AddMenuItem(SORS.Chats.declare_attack .. SORS.Chats._to .. SORS.Chats.warband,
				SORRealms.BroadcastW, false, true, EA_Window_ContextMenu.BARMENU)
			elseif channel == 1 then
				EA_Window_ContextMenu.AddMenuItem(SORS.Chats.declare_attack .. SORS.Chats._to .. SORS.Chats.party, 
				SORRealms.BroadcastP, false, true, EA_Window_ContextMenu.BARMENU)
			end	
			if SOR.IsChannel(1) then
			EA_Window_ContextMenu.AddMenuItem(SORS.Chats.declare_attack .. SORS.Chats._to .. SORS.Chats.region,
			SORRealms.Broadcast1, false, true, EA_Window_ContextMenu.BARMENU)
			end
			if SOR.IsChannel(2) then
			EA_Window_ContextMenu.AddMenuItem(SORS.Chats.declare_attack .. SORS.Chats._to .. SORS.Chats.rvr,
			SORRealms.Broadcast2, false, true, EA_Window_ContextMenu.BARMENU)
			end
	elseif 	keepData.attackStatus ~= 1 then
		Broadcast = SORS.SORRealms.heading_to .. SORS.KeepMap[keepData.id] .. SORS.SORRealms.to_defend ..  SORS.KeepNames[keepData.id] 
		.. SORS.SORRealms.from_an_attack_by .. SORS.Opposition[GameData.Player.realm]
		
		if SOR_SavedSettings["CustomChat"] then
				EA_Window_ContextMenu.AddMenuItem(SORS.Chats.declare_defence .. SORS.Chats._to .. SOR_SavedSettings["CustomChat"],
				SORRealms.BroadcastC, false, true, EA_Window_ContextMenu.BARMENU)
		end
		
			if channel == 2 then
				EA_Window_ContextMenu.AddMenuItem(SORS.Chats.declare_defence .. SORS.Chats._to .. SORS.Chats.warband,
				SORRealms.BroadcastW, false, true, EA_Window_ContextMenu.BARMENU)
			elseif channel == 1 then
				EA_Window_ContextMenu.AddMenuItem(SORS.Chats.declare_defence .. SORS.Chats._to .. SORS.Chats.party,
				SORRealms.BroadcastP, false, true, EA_Window_ContextMenu.BARMENU)
			end	
				if SOR.IsChannel(1) then
				EA_Window_ContextMenu.AddMenuItem(SORS.Chats.declare_defence .. SORS.Chats._to .. SORS.Chats.region,
				SORRealms.Broadcast1, false, true, EA_Window_ContextMenu.BARMENU)
				end
				if SOR.IsChannel(2) then
				EA_Window_ContextMenu.AddMenuItem(SORS.Chats.declare_defence .. SORS.Chats._to .. SORS.Chats.rvr,
				SORRealms.Broadcast2, false, true, EA_Window_ContextMenu.BARMENU)
				end
	end
	
	Info = SORS.KeepNames[keepData.id] .. SORS.SORRealms._in .. SORS.KeepMap[keepData.id]
	if keepData.attackStatus ~= 1 then
		Info = Info .. SORS.SORRealms.is_under_attack_by_the_forces_of .. SORS.Opposition[keepData.realmOwner]
	else
		Info = Info .. SORS.SORRealms.is_controlled_by .. SORS.Side[keepData.realmOwner] .. SORS.SORRealms.and_is_safe
	end
	
		if SOR_SavedSettings["CustomChat"] then
				EA_Window_ContextMenu.AddMenuItem(SORS.Chats.broadcast_info .. SORS.Chats._to .. SOR_SavedSettings["CustomChat"],
				SORRealms.BroadcastInfoC, false, true, EA_Window_ContextMenu.BARMENU)
		end
	
			if channel == 2 then
				EA_Window_ContextMenu.AddMenuItem(SORS.Chats.broadcast_info .. SORS.Chats._to .. SORS.Chats.warband,
				SORRealms.BroadcastInfoW, false, true, EA_Window_ContextMenu.BARMENU)
			elseif channel == 1 then
				EA_Window_ContextMenu.AddMenuItem(SORS.Chats.broadcast_info .. SORS.Chats._to .. SORS.Chats.party,
				SORRealms.BroadcastInfoP, false, true, EA_Window_ContextMenu.BARMENU)
			end	
				if SOR.IsChannel(1) then
				EA_Window_ContextMenu.AddMenuItem(SORS.Chats.broadcast_info .. SORS.Chats._to .. SORS.Chats.region,
				SORRealms.BroadcastInfo1, false, true, EA_Window_ContextMenu.BARMENU)
				end
				if SOR.IsChannel(2) then
				EA_Window_ContextMenu.AddMenuItem(SORS.Chats.broadcast_info .. SORS.Chats._to .. SORS.Chats.rvr,
				SORRealms.BroadcastInfo2, false, true, EA_Window_ContextMenu.BARMENU)
				end

				
	EA_Window_ContextMenu.Finalize(EA_Window_ContextMenu.BARMENU)
end

function SORRealms.GetCityLockdown()
	local pairData = GetCampaignPairingData(1)
	local ZoneData = GetCampaignZoneData( 162 )
	if ZoneData.controllingRealm == 0 then
		return GetZoneName(pairData.contestedZone) .. SORS.SORRealms.siege_will_last_for_another .. timeformat(GameData.CityScenarioData.timeLeft) .. SORS.SORRealms.minutes
	elseif ZoneData.controllingRealm == 2 then
		return GetZoneName(pairData.contestedZone) .. SORS.SORRealms.is_in_this_stage_for_another .. timeformat(GameData.CityScenarioData.timeLeft) .. SORS.SORRealms.minutes
	else
		ZoneData = GetCampaignZoneData( 161 )
		if ZoneData.controllingRealm == 0 then
			return GetZoneName(pairData.contestedZone) .. SORS.SORRealms.siege_will_last_for_another .. timeformat(GameData.CityScenarioData.timeLeft) .. SORS.SORRealms.minutes
		elseif ZoneData.controllingRealm == 1 then
			return GetZoneName(pairData.contestedZone) .. SORS.SORRealms.is_in_this_stage_for_another .. timeformat(GameData.CityScenarioData.timeLeft) .. SORS.SORRealms.minutes
		else
			return GetZoneName(pairData.contestedZone) .. SORS.SORRealms.is_resetting_in_another .. timeformat(GameData.CityScenarioData.timeLeft) .. SORS.SORRealms.minutes
		end
	end
end

function SORRealms.BroadcastCityC()
	local pairData = GetCampaignPairingData(1)
	SystemData.UserInput.ChatText = L"/" .. SOR_SavedSettings["CustomChat"] .. L" " .. SORRealms.GetCityLockdown()
	BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)
end

function SORRealms.BroadcastCity1()
	local pairData = GetCampaignPairingData(1)
	SystemData.UserInput.ChatText = L"/1 " .. SORRealms.GetCityLockdown()
	BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)
end

function SORRealms.BroadcastCity2()
	local pairData = GetCampaignPairingData(1)
	SystemData.UserInput.ChatText = L"/2 " .. SORRealms.GetCityLockdown()
	BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)
end


function SORRealms.BroadcastCityP()
	SystemData.UserInput.ChatText = L"/p " .. SORRealms.GetCityLockdown()
	BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)
end

function SORRealms.BroadcastCityW()
	SystemData.UserInput.ChatText = L"/war " .. SORRealms.GetCityLockdown()
	BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)
end

function SORRealms.BroadcastC()
	SystemData.UserInput.ChatText = L"/" .. SOR_SavedSettings["CustomChat"] .. L" " .. Broadcast
	BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)
end
	
function SORRealms.Broadcast1()
	SystemData.UserInput.ChatText = L"/1 " .. Broadcast
	BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)
end

function SORRealms.Broadcast2()
	SystemData.UserInput.ChatText = L"/2 " .. Broadcast
	BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)
end

function SORRealms.BroadcastP()
	SystemData.UserInput.ChatText = L"/p " .. Broadcast
	BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)
end

function SORRealms.BroadcastW()
	SystemData.UserInput.ChatText = L"/war " .. Broadcast
	BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)
end

function SORRealms.BroadcastInfoC()
	SystemData.UserInput.ChatText = L"/" .. SOR_SavedSettings["CustomChat"] .. L" " .. Info
	BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)
end

function SORRealms.BroadcastInfo1()
	SystemData.UserInput.ChatText = L"/1 " .. Info
	BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)
end

function SORRealms.BroadcastInfo2()
	SystemData.UserInput.ChatText = L"/2 " .. Info
	BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)
end

function SORRealms.BroadcastInfoP()
	SystemData.UserInput.ChatText = L"/p " .. Info
	BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)
end

function SORRealms.BroadcastInfoW()
	SystemData.UserInput.ChatText = L"/war " .. Info
	BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)
end

