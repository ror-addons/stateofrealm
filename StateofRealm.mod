<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <UiMod name="SOR" version="2.3" date="2/11/2009" >
  <!-- really version 3.2 -->
	  <Author name="Ratsug" email="mageboltrat@ntlworld.com" />
  	<Description text="Simple and Detailed display of the current tier 4 war effort." />
        
    <Dependencies>
      <Dependency name="EASystem_LayoutEditor" />
      <Dependency name="EASystem_WindowUtils" />
      <Dependency name="EA_AlertTextWindow" />
    </Dependencies>
        
	  <Files>
      <File name="icon.xml" />
      <File name="SORStrings-en.lua" />
	  <File name="SORStrings-de.lua" />
	  <File name="SORData.lua" />
      <File name="SOR.xml" />
	  <File name="SOR.lua" />
	  <File name="SORRealms.xml" />
	  <File name="SORRealms.lua" />
	  <File name="SORPager.xml" />
	   <File name="SORPager.lua" />
	   <File name="SORObjs.xml" />
       <File name="SORObjs.lua" />
	   <File name="SOROptions.xml" />
	   <File name="SOROptions.lua" />


    </Files>

	  <SavedVariables>
			<SavedVariable name="SOR_SavedSettings" />
		</SavedVariables>

	  <OnInitialize>
		      		<CallFunction name="SOR.Initialize" />
					<CallFunction name="SORObjs.Initialize" />
					<CallFunction name="SOROptions.Initialize" />
					local mods = ModulesGetData()
					for k,v in ipairs(mods) do
						if v.name == "LibSlash" then
							if v.isEnabled and not v.isLoaded then
								ModuleInitialize(v.name);

							end
							break
						end
					end
					<CallFunction name="SOROptions.SlashCommands" />
	  </OnInitialize>
  	<OnUpdate>
      		<CallFunction name="SOR.Update" />
		<CallFunction name="SORObjs.OnUpdate" />
    	</OnUpdate>
	  <OnShutdown/>
		
	</UiMod>
</ModuleFile>
