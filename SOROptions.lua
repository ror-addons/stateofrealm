SOROptions = {}

local SORWin = "SOR.Options"
local TextR
local TextG
local TextB
local TierR
local TierG
local TierB
local TierHidden = {}
local AlphaVal
local SwingShift
local NoUpdateTemp

local function print(txt)
    EA_ChatWindow.Print(towstring(txt))
end

local function timeformat(secs)
    local trunc = math.ceil(secs)
    local mins = math.floor((1 + trunc - (trunc % 60)) / 60)
    local secs = trunc % 60
	if mins > 60 then
		local hours = math.floor((1 + mins - (mins % 60)) / 60)
		local mins = mins % 60
	    return wstring.format(L"%d:%02d:%02d", hours, mins, secs)
	end
    return wstring.format(L"%d:%02d", mins, secs)
end

function SOROptions.GetSettings()
	if SOR_SavedSettings["HideRealm"] then ComboBoxSetSelectedMenuItem("SOR.Options.RealmMode.DL", 1)
	elseif SOR_SavedSettings["RealmMode"] == "High" then ComboBoxSetSelectedMenuItem("SOR.Options.RealmMode.DL", 3)
	elseif SOR_SavedSettings["RealmMode"] == "Low" then ComboBoxSetSelectedMenuItem("SOR.Options.RealmMode.DL", 4)
	else  ComboBoxSetSelectedMenuItem("SOR.Options.RealmMode.DL", 2)
	end
	
	if SOR_SavedSettings["HideObj"] then ComboBoxSetSelectedMenuItem("SOR.Options.ObjMode.DL", 1)
	elseif SOR_SavedSettings["FloatObj"] then ComboBoxSetSelectedMenuItem("SOR.Options.ObjMode.DL", 3)
	else ComboBoxSetSelectedMenuItem("SOR.Options.ObjMode.DL", 2)
	end
	
	if SOR_SavedSettings["HidePager"] then ComboBoxSetSelectedMenuItem("SOR.Options.PagerMode.DL", 1)
	elseif SOR_SavedSettings["FloatPager"] then ComboBoxSetSelectedMenuItem("SOR.Options.PagerMode.DL", 3)
	else ComboBoxSetSelectedMenuItem("SOR.Options.PagerMode.DL", 2)
	end

	if SOR_SavedSettings["GrowUp"] then ComboBoxSetSelectedMenuItem("SOR.Options.GrowUp.DL", 1)
	else ComboBoxSetSelectedMenuItem("SOR.Options.GrowUp.DL", 2)
	end
	
	if SOR_SavedSettings["Language"] then
		ComboBoxSetSelectedMenuItem("SOR.Options.Language.DL", SOR_SavedSettings["Language"])
	else
		ComboBoxSetSelectedMenuItem("SOR.Options.Language.DL", 1)
	end
	
	ComboBoxSetSelectedMenuItem("SOR.Options.Skin.DL", SOR_SavedSettings["ColourSet"])
	ComboBoxSetSelectedMenuItem("SOR.Options.Skin2.DL", SOR_SavedSettings["GraphicSet"])
	
	if SOR_SavedSettings["TipLeft"] then ComboBoxSetSelectedMenuItem("SOR.Options.ToolTip.DL", 1)
	else ComboBoxSetSelectedMenuItem("SOR.Options.ToolTip.DL", 2)
	end	
	
	if SOR_SavedSettings["MinimapButton"] then ComboBoxSetSelectedMenuItem("SOR.Options.ButtonMode.DL", 2)
	else ComboBoxSetSelectedMenuItem("SOR.Options.ButtonMode.DL", 1)
	end	
	
	if SOR_SavedSettings["Bottom"] then ComboBoxSetSelectedMenuItem("SOR.Options.AnchorWindow.DL", 2)
	else ComboBoxSetSelectedMenuItem("SOR.Options.AnchorWindow.DL", 1)
	end	

	NoUpdateTemp = SOR_SavedSettings["NoUpdate"]
	if SOR_SavedSettings["NoUpdate"] then ComboBoxSetSelectedMenuItem("SOR.Options.UpdateOn.DL", 2)
	else ComboBoxSetSelectedMenuItem("SOR.Options.UpdateOn.DL", 1)
	end	
	
	
	
	SliderBarSetCurrentPosition("SOR.Options.AlphaVal.slider", SOR_SavedSettings["BackgroundAlpha"])
	SliderBarSetCurrentPosition("SOR.Options.TextR.slider", SOR_SavedSettings["TextR"] / 255)
	SliderBarSetCurrentPosition("SOR.Options.TextG.slider", SOR_SavedSettings["TextG"] / 255)
	SliderBarSetCurrentPosition("SOR.Options.TextB.slider", SOR_SavedSettings["TextB"] / 255)	
	SliderBarSetCurrentPosition("SOR.Options.TierR.slider", SOR_SavedSettings["TierR"] / 255)
	SliderBarSetCurrentPosition("SOR.Options.TierG.slider", SOR_SavedSettings["TierG"] / 255)
	SliderBarSetCurrentPosition("SOR.Options.TierB.slider", SOR_SavedSettings["TierB"] / 255)	
	SliderBarSetCurrentPosition("SOR.Options.SwingTime.slider", SOR_SavedSettings["SwingShift"] / 360)
	LabelSetText("SOR.Options.AlphaVal.info",	towstring(math.ceil(SOR_SavedSettings["BackgroundAlpha"] * 255)))
	LabelSetText("SOR.Options.TextR.info", 	towstring(SOR_SavedSettings["TextR"]))
	LabelSetText("SOR.Options.TextG.info", 	towstring(SOR_SavedSettings["TextG"]))
	LabelSetText("SOR.Options.TextB.info", 	towstring(SOR_SavedSettings["TextB"]))	
	LabelSetText("SOR.Options.TierR.info", 	towstring(SOR_SavedSettings["TierR"]))
	LabelSetText("SOR.Options.TierG.info", 	towstring(SOR_SavedSettings["TierG"]))
	LabelSetText("SOR.Options.TierB.info", 	towstring(SOR_SavedSettings["TierB"]))	
	LabelSetText("SOR.Options.SwingTime.info", 	timeformat(SOR_SavedSettings["SwingShift"] * 10))
	if SOR_SavedSettings["CustomChat"] then
		TextEditBoxSetText("SOR.Options.Custom.box", SOR_SavedSettings["CustomChat"])
	else
		TextEditBoxSetText("SOR.Options.Custom.box", L"")
	end
	
	for tier = 1,4 do
		TierHidden[tier] = SOR_SavedSettings["TierHidden" .. tier]
		if TierHidden[tier] then
			DynamicImageSetTexture("SOR.Options.Tier" .. tier .. ".btn", "SoRblank" , 0 , 0)
		else
			DynamicImageSetTexture("SOR.Options.Tier" .. tier .. ".btn", "SoRicon" , 0 , 0)
		end
	end	
end

function SOROptions.ShowTier()
  local window = SystemData.ActiveWindow.name
  local tier = tonumber(window:sub(17,17))
  TierHidden[tier] = not TierHidden[tier]
	if TierHidden[tier] then
		DynamicImageSetTexture("SOR.Options.Tier" .. tier .. ".btn", "SoRblank" , 0 , 0)
	else
		DynamicImageSetTexture("SOR.Options.Tier" .. tier .. ".btn", "SoRicon" , 0 , 0)
	end
end

function SOROptions.Slide()
		AlphaVal = math.ceil(SliderBarGetCurrentPosition("SOR.Options.AlphaVal.slider") * 255)
		TextR = math.ceil(SliderBarGetCurrentPosition("SOR.Options.TextR.slider") * 255)
		TextG = math.ceil(SliderBarGetCurrentPosition("SOR.Options.TextG.slider") * 255)
		TextB = math.ceil(SliderBarGetCurrentPosition("SOR.Options.TextB.slider") * 255)
		TierR = math.ceil(SliderBarGetCurrentPosition("SOR.Options.TierR.slider") * 255)
		TierG = math.ceil(SliderBarGetCurrentPosition("SOR.Options.TierG.slider") * 255)
		TierB = math.ceil(SliderBarGetCurrentPosition("SOR.Options.TierB.slider") * 255)
		SwingShift = math.ceil(SliderBarGetCurrentPosition("SOR.Options.SwingTime.slider") * 360)
		LabelSetText("SOR.Options.AlphaVal.info",	towstring(AlphaVal))
		LabelSetText("SOR.Options.TextR.info", 	towstring(TextR))
		LabelSetText("SOR.Options.TextG.info", 	towstring(TextG))
		LabelSetText("SOR.Options.TextB.info", 	towstring(TextB))	
		LabelSetText("SOR.Options.TierR.info", 	towstring(TierR))
		LabelSetText("SOR.Options.TierG.info", 	towstring(TierG))
		LabelSetText("SOR.Options.TierB.info", 	towstring(TierB))	
		LabelSetText("SOR.Options.SwingTime.info", timeformat(SwingShift * 10))
end

function SOROptions.Initialize()


	-- Version Notes --
	LabelSetText("SOR.Version.Title", SORS.SOROptions.Version.Title)
	LabelSetText("SOR.Version.VersionNo.label", SORS.SOROptions.Version.VersionNo.label)
	LabelSetText("SOR.Version.Text1", SORS.SOROptions.Version.Text1)
	
	LabelSetText("SOR.Version.Text2", SORS.SOROptions.Version.Text2)

	LabelSetText("SOR.Version.Text3", SORS.SOROptions.Version.Text3)

	LabelSetText("SOR.Version.Text6",	SORS.SOROptions.Version.Text6)

	
	-- Options --
	
	LabelSetText("SOR.Options.Title", L"State of Realm")

	LabelSetText("SOR.Options.Realm.label", SORS.SOROptions.Realm.label)
	LabelSetText( "SOR.Options.RealmMode.label", SORS.SOROptions.RealmMode.label)
	ComboBoxClearMenuItems( "SOR.Options.RealmMode.DL") 
	ComboBoxAddMenuItem( "SOR.Options.RealmMode.DL", SORS.SOROptions.RealmMode.DL1)
	ComboBoxAddMenuItem( "SOR.Options.RealmMode.DL", SORS.SOROptions.RealmMode.DL2)
	ComboBoxAddMenuItem( "SOR.Options.RealmMode.DL", SORS.SOROptions.RealmMode.DL3)
	ComboBoxAddMenuItem( "SOR.Options.RealmMode.DL", SORS.SOROptions.RealmMode.DL4)
	LabelSetText("SOR.Options.SwingTime.label", SORS.SOROptions.SwingTime.label)	
	LabelSetText("SOR.Options.SwingTime.info", SORS.SOROptions.SwingTime.info)		
	LabelSetText("SOR.Options.SwingLabel1", SORS.SOROptions.SwingLabel1)
	LabelSetText("SOR.Options.SwingLabel2", SORS.SOROptions.SwingLabel2)
	LabelSetText("SOR.Options.SwingLabel3", SORS.SOROptions.SwingLabel3)
	
	
	LabelSetText("SOR.Options.Obj.label", SORS.SOROptions.Obj.label)	
	LabelSetText("SOR.Options.ObjMode.label", SORS.SOROptions.ObjMode.label)
	ComboBoxClearMenuItems( "SOR.Options.ObjMode.DL") 
		ComboBoxAddMenuItem( "SOR.Options.ObjMode.DL", SORS.SOROptions.ObjMode.DL1)
		ComboBoxAddMenuItem( "SOR.Options.ObjMode.DL", SORS.SOROptions.ObjMode.DL2)
		ComboBoxAddMenuItem( "SOR.Options.ObjMode.DL", SORS.SOROptions.ObjMode.DL3)
	LabelSetText("SOR.Options.GrowUp.label", SORS.SOROptions.GrowUp.label)
	ComboBoxClearMenuItems( "SOR.Options.GrowUp.DL") 
		ComboBoxAddMenuItem( "SOR.Options.GrowUp.DL", SORS.SOROptions.GrowUp.DL1)
		ComboBoxAddMenuItem( "SOR.Options.GrowUp.DL", SORS.SOROptions.GrowUp.DL2)
	LabelSetText("SOR.Options.TierLabel", SORS.SOROptions.TierLabel)
		LabelSetText("SOR.Options.Tier1.label", SORS.SOROptions.Tier1.label)
		LabelSetText("SOR.Options.Tier2.label", SORS.SOROptions.Tier2.label)
		LabelSetText("SOR.Options.Tier3.label", SORS.SOROptions.Tier3.label)
		LabelSetText("SOR.Options.Tier4.label", SORS.SOROptions.Tier4.label)
		
	LabelSetText("SOR.Options.Pager.label", SORS.SOROptions.Pager.label)	
	LabelSetText("SOR.Options.PagerMode.label", SORS.SOROptions.PagerMode.label)
	ComboBoxClearMenuItems( "SOR.Options.PagerMode.DL") 
		ComboBoxAddMenuItem( "SOR.Options.PagerMode.DL", SORS.SOROptions.PagerMode.DL1)
		ComboBoxAddMenuItem( "SOR.Options.PagerMode.DL", SORS.SOROptions.PagerMode.DL2)
		ComboBoxAddMenuItem( "SOR.Options.PagerMode.DL", SORS.SOROptions.PagerMode.DL3)
		
	LabelSetText("SOR.Options.Layout.label", SORS.SOROptions.Layout.label)	
	LabelSetText("SOR.Options.ToolTip.label", SORS.SOROptions.ToolTip.label)
	ComboBoxClearMenuItems( "SOR.Options.ToolTip.DL") 
		ComboBoxAddMenuItem( "SOR.Options.ToolTip.DL", SORS.SOROptions.ToolTip.DL1)
		ComboBoxAddMenuItem( "SOR.Options.ToolTip.DL", SORS.SOROptions.ToolTip.DL2)	
		LabelSetText("SOR.Options.ButtonMode.label", SORS.SOROptions.ButtonMode.label)
		ComboBoxClearMenuItems( "SOR.Options.ButtonMode.DL") 
		ComboBoxAddMenuItem( "SOR.Options.ButtonMode.DL", SORS.SOROptions.ButtonMode.DL1)
		ComboBoxAddMenuItem( "SOR.Options.ButtonMode.DL", SORS.SOROptions.ButtonMode.DL2)
		LabelSetText("SOR.Options.AnchorWindow.label", SORS.SOROptions.AnchorWindow.label)
		ComboBoxClearMenuItems( "SOR.Options.AnchorWindow.DL") 
		ComboBoxAddMenuItem( "SOR.Options.AnchorWindow.DL", SORS.SOROptions.AnchorWindow.DL1)
		ComboBoxAddMenuItem( "SOR.Options.AnchorWindow.DL", SORS.SOROptions.AnchorWindow.DL2)	
		
	LabelSetText("SOR.Options.ActiveUpdate.label", SORS.SOROptions.ActiveUpdate.label)	
	LabelSetText("SOR.Options.UpdateOn.label", SORS.SOROptions.UpdateOn.label)
	ComboBoxClearMenuItems( "SOR.Options.UpdateOn.DL") 
		ComboBoxAddMenuItem( "SOR.Options.UpdateOn.DL", SORS.SOROptions.UpdateOn.DL1)
		ComboBoxAddMenuItem( "SOR.Options.UpdateOn.DL", SORS.SOROptions.UpdateOn.DL2)
	
	LabelSetText("SOR.Options.PosReset.label", SORS.SOROptions.PosReset.label)	

	
	LabelSetText("SOR.Options.Look.label", SORS.SOROptions.Look.label)	
	LabelSetText("SOR.Options.Language.label", SORS.SOROptions.Language.label)
	ComboBoxClearMenuItems( "SOR.Options.Language.DL") 
		ComboBoxAddMenuItem( "SOR.Options.Language.DL", SORS.SOROptions.Language.DL1)
		ComboBoxAddMenuItem( "SOR.Options.Language.DL", SORS.SOROptions.Language.DL2)	
	
	LabelSetText("SOR.Options.Skin.label", SORS.SOROptions.Skin.label)
	ComboBoxClearMenuItems( "SOR.Options.Skin.DL") 
		ComboBoxAddMenuItem( "SOR.Options.Skin.DL", SORS.SOROptions.Skin.DL1)
		ComboBoxAddMenuItem( "SOR.Options.Skin.DL", SORS.SOROptions.Skin.DL2)	
		ComboBoxAddMenuItem( "SOR.Options.Skin.DL", SORS.SOROptions.Skin.DL3)
		ComboBoxAddMenuItem( "SOR.Options.Skin.DL", SORS.SOROptions.Skin.DL4)
		ComboBoxAddMenuItem( "SOR.Options.Skin.DL", SORS.SOROptions.Skin.DL5)
		ComboBoxAddMenuItem( "SOR.Options.Skin.DL", SORS.SOROptions.Skin.DL6)
		ComboBoxAddMenuItem( "SOR.Options.Skin.DL", SORS.SOROptions.Skin.DL7)
		ComboBoxAddMenuItem( "SOR.Options.Skin.DL", SORS.SOROptions.Skin.DL8)		
	LabelSetText("SOR.Options.Skin2.label", SORS.SOROptions.Skin2.label)
	ComboBoxClearMenuItems( "SOR.Options.Skin2.DL") 
		ComboBoxAddMenuItem( "SOR.Options.Skin2.DL", SORS.SOROptions.Skin2.DL1)
		ComboBoxAddMenuItem( "SOR.Options.Skin2.DL", SORS.SOROptions.Skin2.DL2)
	--	ComboBoxAddMenuItem( "SOR.Options.Skin2.DL", SORS.SOROptions.Skin2.DL3)		
	LabelSetText("SOR.Options.Text.label", SORS.SOROptions.Text.label)	
	LabelSetText("SOR.Options.TextR.label", SORS.SOROptions.TextR.label)
	LabelSetText("SOR.Options.TextG.label", SORS.SOROptions.TextG.label)
	LabelSetText("SOR.Options.TextB.label", SORS.SOROptions.TextB.label)	
	LabelSetText("SOR.Options.Tier.label", SORS.SOROptions.Tier.label)	
	LabelSetText("SOR.Options.TierR.label", SORS.SOROptions.TierR.label)
	LabelSetText("SOR.Options.TierG.label", SORS.SOROptions.TierG.label)
	LabelSetText("SOR.Options.TierB.label", SORS.SOROptions.TierB.label)	

	LabelSetText("SOR.Options.AlphaLabel.label", SORS.SOROptions.AlphaLabel.label)
	LabelSetText("SOR.Options.AlphaVal.label", SORS.SOROptions.AlphaVal.label)	

	ButtonSetText("SOR.Options.Save", SORS.SOROptions.Save)
	ButtonSetText("SOR.Options.Reset", SORS.SOROptions.Reset)	
	
	LabelSetText("SOR.Options.Broadcast.label", SORS.SOROptions.Broadcast.label)			
	LabelSetText("SOR.Options.Custom.label", SORS.SOROptions.Custom.label)
	LabelSetText("SOR.Options.Custom.sub", SORS.SOROptions.Custom.sub)
	SOROptions.GetSettings()
		
end

function SOROptions.Save()
	if ComboBoxGetSelectedMenuItem("SOR.Options.UpdateOn.DL") == 1 then
		SOR_SavedSettings["NoUpdate"] = false
	else
		SOR_SavedSettings["NoUpdate"] = true
	end
	if NoUpdateTemp ~= SOR_SavedSettings["NoUpdate"] then
		if SOR_SavedSettings["NoUpdate"] then
			SORPager.UnLogChannel()
			d("unlog")
		else
			SORPager.LogChannel()		
			d("log")
		end
		NoUpdateTemp = SOR_SavedSettings["NoUpdate"]
	end

	for i = 1,4 do
	SOR_SavedSettings["TierHidden" .. i] = TierHidden[i]
	end
	
	if ComboBoxGetSelectedMenuItem("SOR.Options.GrowUp.DL") ~= 0 then
		if ComboBoxGetSelectedMenuItem("SOR.Options.GrowUp.DL") == 1 then SOR_SavedSettings["GrowUp"] = true
		else SOR_SavedSettings["GrowUp"] = false 
		end
	end

	SOR_SavedSettings["Language"] = ComboBoxGetSelectedMenuItem("SOR.Options.Language.DL")
	SOR_SavedSettings["ColourSet"] = ComboBoxGetSelectedMenuItem("SOR.Options.Skin.DL")
	SOR_SavedSettings["GraphicSet"] = ComboBoxGetSelectedMenuItem("SOR.Options.Skin2.DL")
	
	if ComboBoxGetSelectedMenuItem("SOR.Options.ToolTip.DL") == 1 then
		SOR_SavedSettings["TipLeft"] = true
	else
		SOR_SavedSettings["TipLeft"] = false
	end
	
	if ComboBoxGetSelectedMenuItem("SOR.Options.RealmMode.DL") == 1 then 
		SOR_SavedSettings["HideRealm"] = true
	elseif ComboBoxGetSelectedMenuItem("SOR.Options.RealmMode.DL") == 2 then
		SOR_SavedSettings["HideRealm"] = false
		SOR_SavedSettings["RealmMode"] = false
	elseif ComboBoxGetSelectedMenuItem("SOR.Options.RealmMode.DL") == 3 then
		SOR_SavedSettings["HideRealm"] = false
		SOR_SavedSettings["RealmMode"] = "High"
	elseif ComboBoxGetSelectedMenuItem("SOR.Options.RealmMode.DL") == 4 then
		SOR_SavedSettings["HideRealm"] = false
		SOR_SavedSettings["RealmMode"] = "Low"
	end

	if ComboBoxGetSelectedMenuItem("SOR.Options.ObjMode.DL") == 1 then 
		 SOR_SavedSettings["HideObj"] = true
	else
		 SOR_SavedSettings["HideObj"] = false
		if ComboBoxGetSelectedMenuItem("SOR.Options.ObjMode.DL") == 2 then
			SOR_SavedSettings["FloatObj"] = false
		else
			SOR_SavedSettings["FloatObj"] = true
		end
	end


	if ComboBoxGetSelectedMenuItem("SOR.Options.PagerMode.DL") == 1 then 
		 SOR_SavedSettings["HidePager"] = true
	else
		 SOR_SavedSettings["HidePager"] = false
		if ComboBoxGetSelectedMenuItem("SOR.Options.PagerMode.DL") == 2 then
			SOR_SavedSettings["FloatPager"] = false
		else
			SOR_SavedSettings["FloatPager"] = true
		end
	end
		
	if ComboBoxGetSelectedMenuItem("SOR.Options.ButtonMode.DL") == 1  then 
	SOR_SavedSettings["MinimapButton"] = false
	else 
	SOR_SavedSettings["MinimapButton"] = true
	end	
	
	if ComboBoxGetSelectedMenuItem("SOR.Options.AnchorWindow.DL") == 1  then 
	SOR_SavedSettings["Bottom"] = false
	else 
	SOR_SavedSettings["Bottom"] = true
	end	
	
	SOR_SavedSettings["CustomChat"] = TextEditBoxGetText("SOR.Options.Custom.box")
	if #tostring(SOR_SavedSettings["CustomChat"]) == 0 then SOR_SavedSettings["CustomChat"] = false end
	
		SOR_SavedSettings["BackgroundAlpha"] = (SliderBarGetCurrentPosition("SOR.Options.AlphaVal.slider"))
		SOR_SavedSettings["TextR"] = math.ceil(SliderBarGetCurrentPosition("SOR.Options.TextR.slider") * 255)
		SOR_SavedSettings["TextG"] = math.ceil(SliderBarGetCurrentPosition("SOR.Options.TextG.slider") * 255)
		SOR_SavedSettings["TextB"] = math.ceil(SliderBarGetCurrentPosition("SOR.Options.TextB.slider") * 255)
		SOR_SavedSettings["TierR"] = math.ceil(SliderBarGetCurrentPosition("SOR.Options.TierR.slider") * 255)
		SOR_SavedSettings["TierG"] = math.ceil(SliderBarGetCurrentPosition("SOR.Options.TierG.slider") * 255)
		SOR_SavedSettings["TierB"] = math.ceil(SliderBarGetCurrentPosition("SOR.Options.TierB.slider") * 255)
		SOR_SavedSettings["SwingShift"] = math.ceil(SliderBarGetCurrentPosition("SOR.Options.SwingTime.slider") * 360)

	SOR.SetLanguage()		
	SOR.MinimapButton()
	SORRealms.AlphaUpdate()
	SOR.timeri()
	SORRealms.Setup()
	SOR.SetupWindows()
end

function SOROptions.SlashCommands()
	LibSlash.RegisterSlashCmd("sor", function() SOR.OpenOptions() end)
end

function SOROptions.timer1()
	SOROptions.settimer(6)
end

function SOROptions.timer2()
	SOROptions.settimer(30)
end

function SOROptions.timer3()
	SOROptions.settimer(90)
end

function SOROptions.settimer(SwingTime)
	SliderBarSetCurrentPosition("SOR.Options.SwingTime.slider", SwingTime / 360)
	LabelSetText("SOR.Options.SwingTime.info", 	timeformat(SwingTime * 10))
end
