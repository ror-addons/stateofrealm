SORStringsDE = {}

SORStringsDE.KeepNames = {
  [1]=L"Dok Karaz",
  [2]=L"Zahnbrecha-Sumpf",
  [3]=L"Gnol Baraz",
  [4]=L"Dickdreck-Grube",
  [5]=L"Karaz Drengi",
  [6]=L"Kazad Dammaz",
  [7]=L"Blutfaust-Fels",
  [8]=L"Karak Karag",
  [9]=L"Eisnehaut Skar",
  [10]=L"D�stermond Loch",

  [11]=L"Mandreds Stellung",
  [12]=L"Steintroll-Burg",
  [13]=L"Burg Passwacht",
  [14]=L"Burg Steinklaue",
  [15]=L"Wilhelms Faust",
  [16]=L"Morrs Ruhe",
  [17]=L"S�dliche Garnison",
  [18]=L"Garnison der Sch�del",
  [19]=L"Zimmerons Stellung",
  [20]=L"Charons Zitadelle",


  [21]=L"Kaskaden des Donners",
  [22]=L"Zonresgriff",
  [23]=L"Brunnen des Qhaysh",
  [24]=L"Ghronds Sakristei",
  [25]=L"Laube des Lichts",
  [26]=L"S�ulen des Gedenkens",
  [27]=L"Pakt der Flamme",
  [28]=L"Gei�el des Drachenbrechers",
  [29]=L"Weg des Hasses",
  [30]=L"Entschlossenheit der Wut"
}

SORStringsDE.KeepMap = {
  [1]=L"Barak Varr",
  [2]=L"S�mpfe des Vergessens",
  [3]=L"Nachtfeuerpass",
  [4]=L"D�sterlande",
  [5]=L"Kadrintal",
  [6]=L"Kadrintal",
  [7]=L"Donnerberg",
  [8]=L"Donnerberg",
  [9]=L"Schwarzfels",
  [10]=L"Schwarzfels",

  [11]=L"Ostland",
  [12]=L"Land der Trolle",
  [13]=L"Talabecland",
  [14]=L"Nordpass",
  [15]=L"Reikland",
  [16]=L"Reikland",
  [17]=L"Praag",
  [18]=L"Praag",
  [19]=L"Chaosw�ste",
  [20]=L"Chaosw�ste",


  [21]=L"Ellyrion",
  [22]=L"Schattenl�nder",
  [23]=L"Saphery",
  [24]=L"Avelorn",
  [25]=L"Eataine",
  [26]=L"Eataine",
  [27]=L"Drachenwacht",
  [28]=L"Drachenwacht",
  [29]=L"Caledor",
  [30]=L"Caledor"
}


SORStringsDE.KeepAttackStatus = {
  [1]=L"Keep ist sicher.",
  [2]=L"Au�entor wird angegriffen!",
  [3]=L"Innentor wird angegriffen!",
  [4]=L"Innenkeep wird angegriffen!",
  [5]=L"Keep eingenommen!"
}

	
SORStringsDE.Side = {
	[0]=L"Niemand",
	[1]=L"Ordnung",
	[2]=L"Zerst�rung",
	[3]=L"Niemand",
}

SORStringsDE.Opposition = {
	[0]=L"Beide Seiten",
	[1]=L"Zerst�rung",
	[2]=L"Ordnung",
	[3]=L"Beide Seiten",
}

SORStringsDE.OBJBoon = {
	[1] = L"Gabe des Handwerks",
	[2] = L"Gabe des Handels",
	[3] = L"Defensiver Segen",
	[4] = L"Heilender Segen"
}

SORStringsDE.Pairings = {
	[1] = L"Zwerge gegen Gr�nh�ute",
	[2] = L"Imperium gegen Chaos",
	[3] = L"Hochelfen gegen Dunkelelfen"
}

-- Taken from the luas

SORStringsDE.Chats = {
  broadcast = L"Nachricht",
  _to = L" an ",
  warband = L"Kriegstrupp",
  party = L"Gruppe",
  region = L"1: Region",
  rvr = L"2: RvR",
  send_target = L"Ziel senden",
  broadcast_info = L"Info senden",
  send_info = L"Info senden",
  declare_attack = L"Angriff",
  declare_defence = L"Verteidigung",
}

SORStringsDE.SOR = {
  toggle_the_state_of_the_realm = L"Realm-Status anzeigen/verstecken",
}

SORStringsDE.SORObjs = {
  contested_by = L"Angegriffen von ",
  will_be_locked_down_by = L" wird gelockt durch die " ,
  _in = L" in ",
  locked_by = L"Gelockt durch die ",
  is_locked_by = L" ist gelockt durch die ",
  for_another = L" f�r weitere ",
  minutes = L" Min.",
}

SORStringsDE.SOROptions = {
	Version = {
	  Title = L"SoR Versionshinweise",
	  VersionNo = {
	    label = L"State Of Realm 3 Beta"
	  },
	  Text1 = L"Hallo State of Realm Benutzer, Willkommen zur \n" ..
	    L"State of Realms v3 Beta. Diese Version bringt euch \n" ..
	    L"einige der von euch gew�nschten Funktionen.",
	  Text2 = L"Auf einem Server k�nnen die einzelnen SoR-Clients \n" ..
	    L"nun Information untereinander austauschen - \n" ..
	    L"das bedeutet, sobald man eine Zone betritt, \n" ..
	    L"in der sich  bereits jemand mit SoR befindet, \n" ..
	    L"werden die Daten in eurem  SoR automatisch \n" ..
	    L"aktualisiert.",
  	Text3 = L"Der Objective-Tracker zeigt nun die BOs \n" .. 
	    L"f�r alle Zonen (wenn sich jemand dort befindet, \n" ..
	    L"um die �nderungen zu kommunizieren). \n" ..
	    L"In den Optionen  k�nnt ihr den Objective-Tracker \n" ..
	    L"f�r Tiers, die ihr nicht sehen m�chtet, auch \n" ..
	    L"deaktivieren.",
	  Text6 = L"Viel Spa� mit den neuesten Erweiterungen w�nscht \n" ..
	    L"Euer Ratsug."
  },

  Title = L"State of Realm",
  Realm = {
    label = L"Realm-�bersicht"
  },
  RealmMode = {
    label = L"Anzeige",
    DL1 = L"Versteckt",
    DL2 = L"Alle Tiers",
    DL3 = L"Nur Tier 4",
    DL4 = L"Tier 1,2,3"
  },
  SwingTime = {
    label = L"Wechsel der Zonenkontrolle Timer",	
    info = L"01:00"
  },
  SwingLabel1 = L"1 Min",
  SwingLabel2 = L"5 Min",
  SwingLabel3 = L"15 Min",
  Obj = {
    label = L"BO-Tracker"
  },
  ObjMode = {
    label = L"BO-Modus",
    DL1 = L"Versteckt",
    DL2 = L"Gekoppelt",
    DL3 = L"Entkoppelt"
  },
  GrowUp = {
    label = L"Fortschrittsbalken",
    DL1 = L"Aufw�rts",
    DL2 = L"Abw�rts"
  },
	TierLabel = L"Tiers",
  Tier1 = {
    label = L"1"
  },
  Tier2 = {
    label = L"2"
  },
  Tier3 = {
    label = L"3"
  },
  Tier4 = {
    label = L"4"
  },
  Pager = {
    label = L"Pager"
  },
  PagerMode = {
    label = L"Pager-Modus",
    DL1 = L"Versteckt",
    DL2 = L"Gekoppelt",
    DL3 = L"Entkoppelt"
  },
  Layout = {
    label = L"Layout"
  },
  ToolTip = {
    label = L"Tool-Tipps",
    DL1 = L"Links",
    DL2 = L"Rechts"
  },
  ButtonMode = {
    label = L"Button-Modus",
    DL1 = L"Normal",
    DL2 = L"Minimap"
  },
  AnchorWindow = {
    label = L"Verankerung",
    DL1 = L"Oben",
    DL2 = L"Unten"
  },
	ActiveUpdate = {
	  label = L"Active-Update"
	},
	UpdateOn = {
	  label = L"Update-Kanal",
		DL1 = L"An",
		DL2 = L"Aus"
  },
  PosReset = {
    label = L"Standardgr��e & -position setzen"
  },
  Look = {
    label = L"Aussehen"
  },
  Skin = {
    label = L"Farbschema",
    DL1 = L"Reich",
    DL2 = L"Klassisch",
		DL3 = L"Smaragd",
		DL4 = L"Alptraum",
		DL5 = L"Vampir",
		DL6 = L"Amethyst",
		DL7 = L"Aubergine",
		DL8 = L"Pur"
  },
  Skin2 = {
    label = L"SoR-Skin",
		DL1 = L"Original",
		DL2 = L"Light",
		DL3 = L"Custom" 
  },
  Language = {
  label = L"Sprache w�hlen",
    DL1 = L"Englisch",
    DL2 = L"Deutsch"
  },
  Text = {
    label = L"Textfarbe"
  },
  TextR = {
    label = L"Rot"
  },
  TextG = {
    label = L"Gr�n"
  },
  TextB = {
    label = L"Blau"
  },
  Tier = {
    label = L"Tier-Farbe"
  },
  TierR = {
    label = L"Rot"
  },
  TierG = {
    label = L"Gr�n"
  },
  TierB = {
    label = L"Blau"
  },
  AlphaLabel = {
    label = L"Hintergrunddeckkraft"
  },
  AlphaVal = {
    label = L"Alpha"
  },
  Save = L"OK",
  Reset = L"Reset",	
  Broadcast = {
    label = L"Sendeoptionen"
  },
  Custom = {
    label = L"Eigener Kanal",
    sub = L"z.B.: g, ao, sagen"
  }
}

SORStringsDE.SORPager = {
  ChatData_match = L"Langsamer!",
  Boons = {
    AG = L"HW",
    MG = L"HD",
    DB = L"DS",
    HB = L"HS"
  },
  controlled_by = L"Kontrolliert von der ",
  not_controlled = L"Ohne Kontrolle",
  open_for_capture = L"Frei zur Eroberung",
  locked = L"Gelockt",
  contested = L"Angegriffen",
  status_unknown = L"Status unbekannt",
  is_controlled_by = L" wird kontrolliert von der ",
  but_i_dont_have_a_timer_on_it = L" aber es gibt daf�r keinen Timer.",
  no_bos_in_this_zone = L"Keine BOs in dieser Zone",
  open_for_attack_by_the_forces_of = L" Frei zur Eroberung durch die ",
  is_open_for_attack_by_the_forces_of = L"ist frei zur Eroberung durch die ",
  will_be_locked_down_by = L" wird gelockt von der ",
  _in = L" in ",
  is_locked_by = L" ist gelockt von der ",
  for_another = L" f�r weitere ",
  moving_on_to = L" Ziehen weiter zu ",
  _next = L" als n�chstes",
  minutes = L" Min.",
}

SORStringsDE.SORRealms = {
  under_siege = L"Wird belagert",
  altdorf = L"Altdorf",
  altdorf_taken = L"Altdorf eingenommen",
  inevitable_city = L"Unvermeidliche Stadt",
  inevitable_city_taken = L"Unvermeidliche Stadt eingenommen",
  safe = L"Sicher",
  locked_down = L"Gelockt",
  the = L"Die ",
  is_locked = L" ist gelockt",
  realm_taken = L"Reich eingenommen",
  has_been_taken_by = L" wurde eingenommen durch ",
  is_under_attack_by = L" wird angegriffen von der ",
  has = L" hat ",
  percent_control_of_zone = L"% Zonenkontrolle",
  with_a = L" mit einem ",
  percent_swing_to = L"% Anstieg f�r ",
  percent_swing_from = L"% Verlust f�r ",
  no_change = L"Keine �nderung",
  in_the_last = L" in den letzten ",
  percent_control_of = L"% Kontrolle in ",
  percent_control = L"% Kontrolle",
  locked_by = L"Gelocked durch ",
  contested = L"Angegriffen",
  not_owned = L"Nicht in Besitz",
  controlled_by = L"Kontrolliert von der ",
  heading_to = L"Ziehen weiter nach ",
  to_take = L" zur Eroberung von ",
  from = L" von der ",
  to_defend = L" zur Verteidigung von ",
  from_an_attack_by = L" vor einem Angriff der ",
  _in = L" in ",
  is_under_attack_by_the_forces_of = L" wird angegriffen von der ",
  is_controlled_by = L" wird kontrolliert durch die ",
  and_is_safe = L" und ist sicher",
  siege_will_last_for_another = L" Belagerungsdauer betr�gt weitere ",
  is_in_this_stage_for_another = L" befindet sich in diesem Status weitere ",
  is_resetting_in_another = L" wird zur�ckgesetzt in ",
  minutes = L" Min.",
}

SORStringsDE.Info = {
	[2] = {name = L"Der Sp�hposten", objtype = 1, tier = 1, pair = 1, map = L"Bluthornberg", zone = 0 },
	[1] = {name = L"Die Kanonenbatterie", objtype = 2, tier = 1, pair = 1, map = L"Ekrund", zone = 0 },
	[5099] = {name = L"Der Steinmienen-Turm", objtype = 3, tier = 1, pair = 1, map = L"Ekrund", zone = 0 },
	[5098] = {name = L"Eisenm�hnen Au�enposten", objtype = 4, tier = 1, pair = 1, map = L"Bluthornberg", zone = 0 },
	
	[228] = {name = L"Die Nordland XI", objtype = 1, tier = 1, pair = 2, map = L"Nordland", zone = 0 },
	[229] = {name = L"Die verlorene Lagune", objtype = 2, tier = 1, pair = 2, map = L"Norsca", zone = 0 },
	[5097] = {name = L"Der Festplatz", objtype = 3, tier = 1, pair = 2, map = L"Nordland", zone = 0 },
	[5096] = {name = L"Der Ernteschrein", objtype = 4, tier = 1, pair = 2, map = L"Nordland", zone = 0 },

	[5072] = {name = L"Der Splitter des Grams", objtype = 1, tier = 1, pair = 3, map = L"Chrace", zone = 0 },
	[5073] = {name = L"Der Turm der Nachtflamme", objtype = 2, tier = 1, pair = 3, map = L"Chrace", zone = 0 },
	[5075] = {name = L"Der Altar des Khaine", objtype = 3, tier = 1, pair = 3, map = L"Insel des Unheils", zone = 0 },
	[5074] = {name = L"Das Haus von Lorendyth", objtype = 4, tier = 1, pair = 3, map = L"Insel des Unheils", zone = 0 },	
	
	
	[57] = {name = L"Der Leuchtturm", objtype = 1, tier = 2, pair = 1, map = L"Barak Varr", zone = 0 },
	[61] = {name = L"Die Goblin-Waffenkammer", objtype = 2, tier = 2, pair = 1, map = L"S�mpfe des Vergessens", zone = 0 },
	[62] = {name = L"Alcadizaars Grab", objtype = 3, tier = 2, pair = 1, map = L"S�mpfe des Vergessens", zone = 0 },
	[135] = {name = L"Das Panzerschiff", objtype = 4, tier = 2, pair = 1, map = L"Barak Varr", zone = 0 },
	
	[241] = {name = L"Die Krypta der Waffen", objtype = 1, tier = 2, pair = 2, map = L"Ostland", zone = 0 },
	[239] = {name = L"Die Ruinen von Burg Graustein", objtype = 2, tier = 2, pair = 2, map = L"Land der Trolle", zone = 0 },
	[240] = {name = L"Kloster des Morr", objtype = 3, tier = 2, pair = 2, map = L"Land der Trolle", zone = 0 },
	[242] = {name = L"Kinschels Hochburg", objtype = 4, tier = 2, pair = 2, map = L"Ostland", zone = 0 },
	
	[5070] = {name = L"Der Schattenturm", objtype = 1, tier = 2, pair = 3, map = L"Schattenland", zone = 0 },
	[5071] = {name = L"Das Einhorn-Belagerungslager", objtype = 2, tier = 2, pair = 3, map = L"Schattenland", zone = 0 },
	[5068] = {name = L"Die Grenzreiterst�lle", objtype = 3, tier = 2, pair = 3, map = L"Ellyrion", zone = 0 },
	[5069] = {name = L"Die Nadel von Ellyrion", objtype = 4, tier = 2, pair = 3, map = L"Ellyrion", zone = 0 },
	
	
	[90] = {name = L"Karagaz", objtype = 1, tier = 3, pair = 1, map = L"D�sterlande", zone = 0 },
	[100] = {name = L"Bugmans Brauerei", objtype = 2, tier = 3, pair = 1, map = L"Nachtfeuerpass", zone = 0 },
	[76] = {name = L"Furrigs Senke", objtype = 3, tier = 3, pair = 1, map = L"Nachtfeuerpass", zone = 0 },
	[101] = {name = L"Goblin Artillery Range", objtype = 4, tier = 3, pair = 1, map = L"D�sterlande", zone = 0 },
	
	[5090] = {name = L"Das Hallenfurt-Herrenhaus", objtype = 1, tier = 3, pair = 2, map = L"Nordpass", zone = 0 },
	[5094] = {name = L"Verentanes Turm", objtype = 2, tier = 3, pair = 2, map = L"Talabecland", zone = 0 },
	[5091] = {name = L"Feitens Sperre", objtype = 3, tier = 3, pair = 2, map = L"Nordpass", zone = 0 },
	[5092] = {name = L"Ogrunds Taverne", objtype = 4, tier = 3, pair = 2, map = L"Nordpass", zone = 0 },
	
	[5066] = {name = L"Holf�lla-Laga", objtype = 1, tier = 3, pair = 3, map = L"Avelorn", zone = 0 },
	[5065] = {name = L"Die Landung der Maid", objtype = 2, tier = 3, pair = 3, map = L"Avelorn", zone = 0 },
	[5063] = {name = L"Sari' Daroir", objtype = 3, tier = 3, pair = 3, map = L"Saphery", zone = 0 },
	[5064] = {name = L"Der Turm des Teclis", objtype = 4, tier = 3, pair = 3, map = L"Saphery", zone = 0 },
	
	
	
	[153] = {name = L"Hartwasserf�lle", objtype = 1, tier = 4, pair = 1, map = L"Kadrintal", zone = 2 },
	[158] = {name = L"Dolgrunds H�gelgrab", objtype = 2, tier = 4, pair = 1, map = L"Kadrintal", zone = 2 },
	[157] = {name = L"Die Gromril-Abzweigung", objtype = 3, tier = 4, pair = 1, map = L"Kadrintal", zone = 2 },
	[156] = {name = L"Eisherd-�bergang", objtype = 4, tier = 4, pair = 1, map = L"Kadrintal", zone = 2 },
	
	[150] = {name = L"Die Unheilsverk�nder-Ader", objtype = 1, tier = 4, pair = 1, map = L"Donnerberg", zone = 3 },
	[147] = {name = L"Karak Palik", objtype = 2, tier = 4, pair = 1, map = L"Donnerberg", zone = 3 },
	[182] = {name = L"Thargrims Frontmauer", objtype = 3, tier = 4, pair = 1, map = L"Donnerberg", zone = 3 },
	[151] = {name = L"Gromril Kruk", objtype = 4, tier = 4, pair = 1, map = L"Donnerberg", zone = 3 },

	[161] = {name = L"Die Aaaaargh-Ernte", objtype = 1, tier = 4, pair = 1, map = L"Schwarzfels", zone = 4 },
	[160] = {name = L"Die Schleuda-M�hle", objtype = 2, tier = 4, pair = 1, map = L"Schwarzfels", zone = 4 },
	[162] = {name = L"Die Fauldorn-Schlucht", objtype = 3, tier = 4, pair = 1, map = L"Schwarzfels", zone = 4 },
	[159] = {name = L"Die squigligen Bestienpferche", objtype = 4, tier = 4, pair = 1, map = L"Schwarzfels", zone = 4 },
	
	
	[5086] = {name = L"Frostbarts Grube", objtype = 1, tier = 4, pair = 2, map = L"Reikland", zone = 2 },
	[5087] = {name = L"Das Schwenderhalle-Herrenhaus", objtype = 2, tier = 4, pair = 2, map = L"Reikland", zone = 2 },
	[5085] = {name = L"Die Reikwacht", objtype = 3, tier = 4, pair = 2, map = L"Reikland", zone = 2 },
	[5088] = {name = L"Die Runenhammer-Kanonenwerke", objtype = 4, tier = 4, pair = 2, map = L"Reikland", zone = 2 },
	
	[5080] = {name = L"Der M�rtyrerplatz", objtype = 1, tier = 4, pair = 2, map = L"Praag", zone = 3 },
	[5084] = {name = L"Anwesen des Ortel von Zaris", objtype = 2, tier = 4, pair = 2, map = L"Praag", zone = 3 },
	[5081] = {name = L"Kurlovs Waffenkammer", objtype = 3, tier = 4, pair = 2, map = L"Praag", zone = 3 },
	[5083] = {name = L"Der Russenscheller Friedhof", objtype = 4, tier = 4, pair = 2, map = L"Praag", zone = 3 },

	[5077] = {name = L"Das Thaugamond-Massiv", objtype = 1, tier = 4, pair = 2, map = L"Chaosw�ste", zone = 4 },
	[5078] = {name = L"Der W�rgdornzweig", objtype = 2, tier = 4, pair = 2, map = L"Chaosw�ste", zone = 4 },
	[5076] = {name = L"Statue des Ewig Auserw�hlten", objtype = 3, tier = 4, pair = 2, map = L"Chaosw�ste", zone = 4 },
	[5079] = {name = L"Der Schrein der Zeit", objtype = 4, tier = 4, pair = 2, map = L"Chaosw�ste", zone = 4 },
	
	
	[632] = {name = L"Das Frostwind-Herrenhaus", objtype = 1, tier = 4, pair = 3, map = L"Eataine", zone = 2 },
	[635] = {name = L"Das Uthorin-Belagerungslager", objtype = 2, tier = 4, pair = 3, map = L"Eataine", zone = 2 },
	[634] = {name = L"Das Heiligtum der Tr�ume" , objtype = 3, tier = 4, pair = 3, map = L"Eataine", zone = 2 },
	[633] = {name = L"Bel-Korhadris' Einsamkeit", objtype = 4, tier = 4, pair = 3, map = L"Eataine", zone = 2 },
	
	[612] = {name = L"Milaiths Erinnerung", objtype = 1, tier = 4, pair = 3, map = L"Drachenwacht", zone = 3 },
	[615] = {name = L"Der Feuerwachen-Turm", objtype = 2, tier = 4, pair = 3, map = L"Drachenwacht", zone = 3 },
	[614] = {name = L"Pelgoraths Glut", objtype = 3, tier = 4, pair = 3, map = L"Drachenwacht", zone = 3 },
	[613] = {name = L"Klagenfeuers Ann�herung", objtype = 4, tier = 4, pair = 3, map = L"Drachenwacht", zone = 3 },

	[636] = {name = L"Der Senlathain-St�tzpunkt", objtype = 1, tier = 4, pair = 3, map = L"Caledor", zone = 4 },
	[639] = {name = L"Die Druchii-Kaserne", objtype = 2, tier = 4, pair = 3, map = L"Caledor", zone = 4 },
	[638] = {name = L"Der Schrein des Eroberers", objtype = 3, tier = 4, pair = 3, map = L"Caledor", zone = 4 },
	[637] = {name = L"Das Sarathanan-Tal", objtype = 4, tier = 4, pair = 3, map = L"Caledor", zone = 4 }
	
	
}
