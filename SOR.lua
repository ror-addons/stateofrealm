if not SOR then SOR = {} end
SORS = SORStringsEN

--Saved Variables
SOR_SavedSettings = {}

--Constants
local VersionNumber = L"v3.2"

local TIME_DELAY = 1
local CHANGE_DELAY = 10

local ZONE_NUMBERS = {[1]=10, [2]=9, [3]=5, [4]=3, [5]=4}

local Throttle = 0
local SendTime = 10
local TimeLeft = 0
local ShiftTimeLeft = 0
local ShiftPosition = 0
local SaveButton = 0
local UnknownTime = 1080


function SOR.SetLanguage()
	if SOR_SavedSettings["Language"] then
		if SOR_SavedSettings["Language"] == 1 then
			SORS = SORStringsEN
		elseif SOR_SavedSettings["Language"] == 2 then
			SORS = SORStringsDE
		end
	end
	SOROptions.Initialize()
	SORPager.SetText()
end

function SOR.Throttle()
	Throttle = Throttle + 5
	SendTime = SendTime + 5
end

function SOR.Unthrottle()
	Throttle = Throttle - 1
	if Throttle < 0 then Throttle = 0 end
end

function SOR.AddTime()
	SendTime = SendTime + 10
end

-- Function that Changes Label Text --
function SOR.SetLabel(label, text)
  LabelSetText(label, text)
  LabelSetTextColor(label, SOR_SavedSettings["TextR"], SOR_SavedSettings["TextG"], SOR_SavedSettings["TextB"])
end

-- Converts System Zone Code to 1-5 Zone Number --
function SOR.ConvertZoneNumber(zone, pairing)
  local number = zone - (100 * (pairing - 1))

  for index,zone in pairs(ZONE_NUMBERS) do
    if(number == zone) then
      return index
    end
  end

  return -1

end

function SOR.ConvertPairing(zone)
  local pairing = 1
	if zone > 200 then
	 pairing = 3	
	elseif  zone > 100 then
	 pairing = 2
	end
	return pairing
end
	
function SOR.GetShiftPosition()
	return ShiftPosition
end

function SOR.SetUnknownTime(newtime)
	UnknownTime = newtime
end

function SOR.CheckUnknownTime(timer)
	if timer < UnknownTime then
		UnknownTime = timer
	end
end

function SOR.GetChannel()
    local channel = 0
    local groupData = GetGroupData()
    for k,v in pairs(groupData) do
        if v and v.name and v.name ~= L"" then
            channel = 1
        end
    end

    if IsWarBandActive() then
        channel = 2
    end
    if GameData.Player.isInScenario and GameData.Player.isInSiege then
        channel = 2
    end
    if GameData.Player.isInScenario and not GameData.Player.isInSiege then
        channel = 0
    end

    return channel
end

function SOR.IsChannel(num)
	local channels = GetChatChannelNames()
	for k,v in pairs(channels) do	
		if v.number == num then return true end
	end
	return false
end


function SOR.GetZone()
	local pairData = GetCampaignPairingData( GetZonePairing())
	local zone = SOR.ConvertZoneNumber(pairData.contestedZone, GetZonePairing())
	if zone == 1 or zone == 5 then
		return 3
	else
		return zone
	end
end

	

------------------------------ Initialization Function -------------------------------------------
function SOR.Initialize()
	
	table.insert(LayoutEditor.EventHandlers, Button.SavePosition)
	LayoutEditor.RegisterEditCallback(SOR.SetupWindows)
	CreateWindow("SOR.Realm", true)
 	CreateWindow("SOR.Button", true)
	CreateWindow("SOR.Options", true)
	CreateWindow("SOR.Obj", true)	
	CreateWindow("SOR.Pager", true)
	CreateWindow("PagerAnchor", true)	
	CreateWindow("RealmAnchor", true)
	CreateWindow("ObjAnchor", true)	
	CreateWindow("SOR.Version", true)	
	
  -- Register the windows with the Layout Editor
	LayoutEditor.RegisterWindow( "RealmAnchor",
	                        L"SoR Main Window",
	                        L"SoR Main Window",
                             false, false,
                             false, nil)

    LayoutEditor.RegisterWindow( "ObjAnchor",
                                L"SoR Objective Timers",
                                L"SoR Objective Timers",
                                false, false,
                                true, nil )	
								
	LayoutEditor.RegisterWindow( "PagerAnchor",
                                L"SoR Pager",
                                L"SoR Pager",
                                false, false,
                                true, nil )	
								
		if SOR_SavedSettings["Language"] then
			if SOR_SavedSettings["Language"] == 1 then
				SORS = SORStringsEN
			elseif SOR_SavedSettings["Language"] == 2 then
				SORS = SORStringsDE
			end
		end
		
		if 	SOR_SavedSettings["VersionNotes"] then 
			WindowSetShowing("SOR.Options", false)
		else
			WindowSetShowing("SOR.Options", true)
			SOR_SavedSettings["VersionNotes"] = VersionNumber
		end
		if not SOR_SavedSettings["Buttonx"] then SOR_SavedSettings["Buttonx"] = 0 end
		if not SOR_SavedSettings["Buttony"] then SOR_SavedSettings["Buttony"] = 0 end	
		if not SOR_SavedSettings["ButtonP"] then SOR_SavedSettings["ButtonP"] = "center" end
		if not SOR_SavedSettings["ButtonRP"] then SOR_SavedSettings["ButtonRP"] = "right" end	
   							
		if not SOR_SavedSettings["TextR"] then SOR_SavedSettings["TextR"] = 255 end
		if not SOR_SavedSettings["TextG"] then SOR_SavedSettings["TextG"] = 255 end
		if not SOR_SavedSettings["TextB"] then SOR_SavedSettings["TextB"] = 255 end
		if not SOR_SavedSettings["TierR"] then SOR_SavedSettings["TierR"] = 255 end
		if not SOR_SavedSettings["TierG"] then SOR_SavedSettings["TierG"] = 255 end
		if not SOR_SavedSettings["TierB"] then SOR_SavedSettings["TierB"] = 255 end		
		if not SOR_SavedSettings["ColourSet"] then SOR_SavedSettings["ColourSet"] = 1 end
		if not SOR_SavedSettings["GraphicSet"] then SOR_SavedSettings["GraphicSet"] = 1 end
		if not SOR_SavedSettings["BackgroundAlpha"] then SOR_SavedSettings["BackgroundAlpha"] = 0.5 end
		if not SOR_SavedSettings["SwingShift"] then SOR_SavedSettings["SwingShift"] = 6 end
		
	if not SOR_SavedSettings["Setup"] then
		SOR.ResetItems()
		SOR_SavedSettings["Setup"] = true
	end
	
	LabelSetText("SOR.Options.V", VersionNumber)
	LabelSetText("SOR.Version.V", VersionNumber)
	
	SORPager.HidePopup()
	SORRealms.AlphaUpdate()							
	SOR.MinimapButton()
	SORRealms.MakeZones()
	SORRealms.Setup()
	SOR.SetupWindows()
	SORPager.Initialize()
	SOR.Update()

end


function Button.SavePosition(eventId)
    if eventId == LayoutEditor.EDITING_END then
		local point, relpoint, relwin, x, y = WindowGetAnchor("SOR.Button", 1)
	    SOR_SavedSettings["ButtonP"] = point
        SOR_SavedSettings["ButtonRP"] = relpoint	
	    SOR_SavedSettings["Buttonx"] = x
        SOR_SavedSettings["Buttony"] = y
		SOR_SavedSettings["ButtonScale"] = WindowGetScale("SOR.Button")
		
	end
end

function SOR.timeri()
	for i = 1, 3 do
		local window = "SOR.Realm" .. i
		SORRealms.Swing(window, i, 0)
	end
end
			
function SOR.OpenOptions() 
  	WindowSetShowing("SOR.Options", not WindowGetShowing("SOR.Options"))
end

function SOR.CloseOptions() 
  	WindowSetShowing("SOR.Options", false)
end

function SOR.CloseNotes() 
  	WindowSetShowing("SOR.Version", false)
end

function SOR.OpenNotes() 
  	WindowSetShowing("SOR.Version", true)
end

function SOR.Toggle()
  SOR_SavedSettings["HideWindow"] = not SOR_SavedSettings["HideWindow"]
  SOR.SetupWindows()
end  

------------------------- FLOAT TOGGLE FUNCTIONS -----------------------

function SOR.Check()
	local point, relpoint, relwin, x, y = WindowGetAnchor("SOR.Realm", 1)
	d(point)
	d(relpoint)
	d(relwin)
	d(x)
	d(y)
end

function SOR.SetupWindows()
	WindowSetDimensions("SOR.Options", 640, 700)
	for pairing = 1,3 do
		for zone = 2,4 do
			DynamicImageSetTexture("SOR.Realm" .. pairing .. ".Zone.Dot" .. zone .. ".image", SOR_SavedSettings["GraphicSet"] .. "Dot" , 0, 0)
		end
			DynamicImageSetTexture("SOR.Realm" .. pairing .. ".Zone.Dot1.Icon", SOR_SavedSettings["GraphicSet"] .. "Flame" , 0, 0)
			DynamicImageSetTexture("SOR.Realm" .. pairing .. ".Zone.Dot5.Icon", SOR_SavedSettings["GraphicSet"] .. "Flame" , 0, 0)				
	end
	
	DynamicImageSetTexture("SOR.Pager.back", SOR_SavedSettings["GraphicSet"] .. "Pager" , 0, 0)
	for dot = 1,4 do
		DynamicImageSetTexture("SOR.Pager" .. dot .. ".Timer.Back", SOR_SavedSettings["GraphicSet"] .. "Timer" , 0, 0)
	end
	
	for bar = 1,5 do
			DynamicImageSetTexture("SOR.Obj" .. bar .. ".Img", SOR_SavedSettings["GraphicSet"] .. "ObjBar" , 0, 0)
	end
	WindowClearAnchors("SOR.Realm")	
	if SOR_SavedSettings["Bottom"] then
		WindowAddAnchor("SOR.Realm", "bottomleft", "RealmAnchor", "bottomleft", 0, 0)
	else
		WindowAddAnchor("SOR.Realm", "topleft", "RealmAnchor" , "topleft", 0, 0)
	end
	WindowClearAnchors("SOR.Obj")
	WindowClearAnchors("SOR.Pager")	
	if SOR_SavedSettings["FloatObj"] then

		if SOR_SavedSettings["GrowUp"] then
			SORObjs.GrowUp()
			WindowAddAnchor("SOR.Obj", "bottomleft", "ObjAnchor", "bottomleft", 0, 0)
		else
			SORObjs.GrowDown()	
			WindowAddAnchor("SOR.Obj", "topleft", "ObjAnchor", "topleft", 0, 0)			
		end
		WindowSetScale("SOR.Obj", WindowGetScale("ObjAnchor"))
	else
		WindowSetScale("SOR.Obj", WindowGetScale("SOR.Realm"))
		if SOR_SavedSettings["Bottom"] == false then
		SORObjs.GrowDown()
			WindowAddAnchor("SOR.Obj", "topleft", "SOR.Realm.ObjAnchor", "topleft", 0, 0)
		else
		SORObjs.GrowUp()
			WindowAddAnchor("SOR.Obj", "topleft", "SOR.Realm.RealmAnchor", "bottomleft", 0, -5)		
		end		
	end
	if SOR_SavedSettings["FloatPager"] then
		WindowSetDimensions("SOR.Realm.PagerAnchor", 220,  0)
		WindowSetScale("SOR.Pager", WindowGetScale("PagerAnchor"))
		WindowAddAnchor("SOR.Pager", "topleft", "PagerAnchor", "topleft", 0, 0)
	else
		WindowSetDimensions("SOR.Realm.PagerAnchor", 220,  32)
		WindowSetScale("SOR.Pager", WindowGetScale("SOR.Realm"))
		WindowAddAnchor("SOR.Pager", "topleft", "SOR.Realm.PagerAnchor", "topleft", 0, 0)
	end	
	if SOR_SavedSettings["HideWindow"] then
		WindowSetShowing("SOR.Obj", false)
		WindowSetShowing("SOR.Realm", false)
		WindowSetShowing("SOR.Pager", false)
	else
		WindowSetShowing("SOR.Obj", not SOR_SavedSettings["HideObj"])
		WindowSetShowing("SOR.Realm", not SOR_SavedSettings["HideRealm"])
		if SOR_SavedSettings["HideRealm"] then
				WindowSetDimensions("SOR.Realm.RealmAnchor", 220,  0)
				WindowSetDimensions("SOR.Realm", 220,  50)
		else 
		
			WindowSetShowing("SOR.Realm0", false)
			WindowSetShowing("SOR.Realm.alpha0", false)
			WindowSetDimensions("SOR.Realm0", 220,  0)
			if SOR_SavedSettings["RealmMode"] =="High" then
				for i = 1, 3 do
					WindowSetShowing("SOR.Realm" .. i .. ".LowImage", false)
					WindowSetShowing("SOR.Realm" .. i .. ".Zone", true)
					WindowSetShowing("SOR.Realm" .. i, true)
					WindowSetShowing("SOR.Realm.alpha" .. i, true)
					WindowSetShowing("SOR.Realm" .. i .. ".Low", false)
					DynamicImageSetTexture("SOR.Realm.alpha" .. i .. ".Alpha", SOR_SavedSettings["GraphicSet"] .. "AlphabackHigh" , 0 , 0)
					DynamicImageSetTexture("SOR.Realm.alpha" .. i .. ".Frame", SOR_SavedSettings["GraphicSet"] .. "FrameHigh" , 0 , 0)
					WindowSetDimensions("SOR.Realm" .. i, 220,  52)
					WindowClearAnchors("SOR.Realm" .. i .. ".Zone")	
					WindowAddAnchor("SOR.Realm" .. i .. ".Zone", "topleft", "SOR.Realm" .. i, "topleft", 0, -6)
					WindowClearAnchors("SOR.Realm" .. i .. ".Battlezone")	
					WindowAddAnchor("SOR.Realm" .. i .. ".Battlezone", "top", "SOR.Realm" .. i, "top", 0, 33)
				end
				WindowSetDimensions("SOR.Realm.RealmAnchor", 220,  156)
				WindowSetDimensions("SOR.Realm", 220,  156)
				elseif SOR_SavedSettings["RealmMode"] == "Low" then
				for i = 1, 3 do
					WindowSetShowing("SOR.Realm" .. i .. ".LowImage", true)
					WindowSetShowing("SOR.Realm" .. i .. ".Zone", false)
					WindowSetShowing("SOR.Realm" .. i, true)
					WindowSetShowing("SOR.Realm.alpha" .. i, true)
					WindowSetShowing("SOR.Realm" .. i .. ".Low", true)
					DynamicImageSetTexture("SOR.Realm.alpha" .. i .. ".Alpha", SOR_SavedSettings["GraphicSet"] .. "AlphabackLow" , 0 , 0)
					DynamicImageSetTexture("SOR.Realm.alpha" .. i .. ".Frame", SOR_SavedSettings["GraphicSet"] .. "FrameLow" , 0 , 0)
					WindowSetDimensions("SOR.Realm" .. i, 220,  38)
					WindowClearAnchors("SOR.Realm" .. i .. ".Low")	
					WindowAddAnchor("SOR.Realm" .. i .. ".Low", "top", "SOR.Realm" .. i , "top", 0, -2)
				end
				WindowSetDimensions("SOR.Realm.RealmAnchor", 220,  114)	
				WindowSetDimensions("SOR.Realm", 220,  114)				
				else
				for i = 1, 3 do
					WindowSetShowing("SOR.Realm" .. i .. ".LowImage", false)
					WindowSetShowing("SOR.Realm" .. i .. ".Zone", true)
					WindowSetShowing("SOR.Realm" .. i, true)
					WindowSetShowing("SOR.Realm.alpha" .. i, true)
					WindowSetShowing("SOR.Realm" .. i .. ".Low", true)
					DynamicImageSetTexture("SOR.Realm.alpha" .. i .. ".Alpha", SOR_SavedSettings["GraphicSet"] .. "Alphaback" , 0 , 0)
					DynamicImageSetTexture("SOR.Realm.alpha" .. i .. ".Frame", SOR_SavedSettings["GraphicSet"] .. "Frame" , 0 , 0)
					WindowSetDimensions("SOR.Realm" .. i, 220,  77)
					WindowClearAnchors("SOR.Realm" .. i .. ".Zone")	
					WindowAddAnchor("SOR.Realm" .. i .. ".Zone", "topleft", "SOR.Realm" .. i, "topleft", 0, 1)
					WindowClearAnchors("SOR.Realm" .. i .. ".Battlezone")	
					WindowAddAnchor("SOR.Realm" .. i .. ".Battlezone", "top", "SOR.Realm" .. i, "top", 0, -5)
					WindowClearAnchors("SOR.Realm" .. i .. ".Low")	
					WindowAddAnchor("SOR.Realm" .. i .. ".Low", "top", "SOR.Realm" .. i .. ".Zone", "top", 0, 36)
				end
				WindowSetDimensions("SOR.Realm.RealmAnchor", 220,  231)
				WindowSetDimensions("SOR.Realm", 220,  231)
				end
			if GameData.CityScenarioData.timeLeft > 0 then
				WindowSetShowing("SOR.Realm.alpha0", true)
				DynamicImageSetTexture("SOR.Realm.alpha0.Alpha", SOR_SavedSettings["GraphicSet"] .. "AlphabackHigh" , 0 , 0)
				DynamicImageSetTexture("SOR.Realm.alpha0.Frame", SOR_SavedSettings["GraphicSet"] .. "FrameHigh" , 0 , 0)
				WindowSetShowing("SOR.Realm0", true)
				WindowSetDimensions("SOR.Realm0", 220,  52)
				if SOR_SavedSettings["RealmMode"] == "High" then
					WindowSetDimensions("SOR.Realm.RealmAnchor", 220,  208)
					WindowSetDimensions("SOR.Realm", 220,  208)
				elseif SOR_SavedSettings["RealmMode"] == "Low" then
					WindowSetDimensions("SOR.Realm.RealmAnchor", 220,  166)	
					WindowSetDimensions("SOR.Realm", 220,  166)
				else
					WindowSetDimensions("SOR.Realm.RealmAnchor", 220,  283)
					WindowSetDimensions("SOR.Realm", 220,  283)
				end
			end
		end
		WindowSetShowing("SOR.Pager", not SOR_SavedSettings["HidePager"])
		if SOR_SavedSettings["HidePager"] then
			WindowSetDimensions("SOR.Realm.PagerAnchor", 220,  0)
		end
	end
	
	WindowSetScale("SOR.Realm", WindowGetScale("RealmAnchor"))
	
	 SORRealms.UpdateZoneControl("SOR.Realm1", 1, 0)
	 SORRealms.UpdateZoneControl("SOR.Realm2", 2, 0)
	 SORRealms.UpdateZoneControl("SOR.Realm3", 3, 0)	
end


function SOR.MinimapButton()
		LayoutEditor.UnregisterWindow("SOR.Button")
		if SOR_SavedSettings["MinimapButton"] then
			if SaveButton == 1 then
				SOR.SaveButton()
				SaveButton = 0
			end
			WindowClearAnchors("SOR.Button")
			WindowSetScale("SOR.Button", 1)
			WindowAddAnchor("SOR.Button", "topleft",  "EA_Window_OverheadMapMapFrame", "center", 32, 62)	
		else
		WindowClearAnchors("SOR.Button")		
		WindowAddAnchor("SOR.Button", SOR_SavedSettings["ButtonP"], "Root", SOR_SavedSettings["ButtonRP"], 
		SOR_SavedSettings["Buttonx"], SOR_SavedSettings["Buttony"])
		WindowSetScale("SOR.Button", SOR_SavedSettings["ButtonScale"])
		
		LayoutEditor.RegisterWindow( "SOR.Button",
	                        L"SOR.Button",
	                        L"State of the Realm Button",
	                        false, false,
	                        true, nil )
		SaveButton = 1
		end
end
  
function SOR.SaveButton()
		local point, relpoint, relwin, x, y = WindowGetAnchor("SOR.Button", 1)
	    SOR_SavedSettings["ButtonP"] = point
        SOR_SavedSettings["ButtonRP"] = relpoint	
	    SOR_SavedSettings["Buttonx"] = x
        SOR_SavedSettings["Buttony"] = y
		SOR_SavedSettings["ButtonScale"] = WindowGetScale("SOR.Button")
end

function SOR.MapButtonMouseOver()
    local windowName	= SystemData.ActiveWindow.name

    Tooltips.CreateTextOnlyTooltip (windowName, nil)
    Tooltips.SetTooltipText (1, 1, SORS.SOR.toggle_the_state_of_the_realm)
    Tooltips.SetTooltipColorDef (1, 1, Tooltips.COLOR_BODY)
    Tooltips.Finalize()
    
    local anchor = { Point="left", RelativeTo=windowName, RelativePoint="right", XOffset=0, YOffset=0 }
    Tooltips.AnchorTooltip (anchor)
    Tooltips.SetTooltipAlpha (1)
end

function SOR.GetUnknownTime()
	return UnknownTime
end

-----------------------------------  Update Function -----------------------------------------------
function SOR.Update(elapsed)
    --Throttle the updating
	local updateChange = 0
	if elapsed then
		TimeLeft = TimeLeft - elapsed
		ShiftTimeLeft = ShiftTimeLeft - elapsed
		UnknownTime = UnknownTime - elapsed
		SendTime = SendTime - elapsed
	end
	
    if TimeLeft > 0 then
        return -- cut out early
    end

	SORPager.SetTimes()
	
	if UnknownTime < 0 then
		UnknownTime = 1080
		SORPager.Switch()
	end

	if SendTime < 0 then
	
		if GameData.Player.career.rank < 5 then
			SendTime = 55
		elseif GameData.Player.career.rank < 10 then
			SendTime = 40
		elseif GameData.Player.career.rank < 20 then
			SendTime = 30
		else
			SendTime = 20
		end
		SendTime = SendTime + Throttle
		SORPager.SendDetails()
		SORPager.RemoveOld()
	end
	
	if ShiftTimeLeft < 0 then
	 ShiftTimeLeft =  CHANGE_DELAY
	 if ShiftPosition == 370 then
		ShiftPosition = 1
	 else
		ShiftPosition = ShiftPosition + 1
	 end
	 updateChange = ShiftPosition
	end
    TimeLeft = TIME_DELAY

    --Perform the update
	 SORPager.CheckZone()
	 SOR.UpdatePagerShown()
	 SORRealms.UpdateCity(updateChange)
	 SORRealms.UpdateZoneControl("SOR.Realm1", 1, updateChange)
	 SORRealms.UpdateZoneControl("SOR.Realm2", 2, updateChange)
	 SORRealms.UpdateZoneControl("SOR.Realm3", 3, updateChange)	 
end

function SOR.UpdatePagerShown()
	if SORObjs.GetTier() == 4 then
		local Pairing = GetZonePairing()
		if Pairing and Pairing >= 1 and Pairing <= 3 then
			local pairData = GetCampaignPairingData(Pairing)
			local zone = SOR.ConvertZoneNumber(pairData.contestedZone, Pairing)
			if zone == 1 or zone == 5 then
				for dot = 1,4 do
					WindowSetAlpha("SOR.Pager" .. dot , 0.5)
				end
			else
				for dot = 1,4 do
					WindowSetAlpha("SOR.Pager" .. dot , 1)
				end
			end
		end
	else
		for dot = 1,4 do
			WindowSetAlpha("SOR.Pager" .. dot , 1)
		end	
	end
end


function SOR.ResetItems()
		SOR_SavedSettings["Buttonx"] = 0
		SOR_SavedSettings["Buttony"] = 0
		SOR_SavedSettings["ButtonP"] = "center"
		SOR_SavedSettings["ButtonRP"] = "bottomright"
		SOR_SavedSettings["ButtonScale"] = 1
		SOR_SavedSettings["MinimapButton"] = false
		SOR.MinimapButton()
		WindowClearAnchors("RealmAnchor")
		WindowAddAnchor("RealmAnchor", "center", "Root", "topleft", 0, -120)	
		WindowClearAnchors("SOR.Realm")
		WindowAddAnchor("SOR.Realm", "topleft", "RealmAnchor", "topleft", 0, 0)	
		WindowClearAnchors("PagerAnchor")
		WindowAddAnchor("PagerAnchor", "bottom", "SOR.Realm", "top", 0, 0)	
		WindowClearAnchors("ObjAnchor")
		WindowAddAnchor("ObjAnchor", "bottom", "SOR.Pager", "top", 0, 0)	

		WindowSetScale("RealmAnchor", 1)
		WindowSetScale("SOR.Realm", 1)
		WindowSetScale("PagerAnchor", 1)
		WindowSetScale("SOR.Pager", 1)	
		WindowSetScale("ObjAnchor", 1)	
		WindowSetScale("SOR.Obj", 1)	
		
		SOR_SavedSettings["Bottom"] = false
		SOROptions.GetSettings()
end


