SORPager = {}

local FirstLoad = true
local LastZone = 0
local BroadcastPager = {}
local PagerVals = {}
local ExternalVals = {}
local ZoneChangeThrottle = 0
local RemovedChannels = {}

function SORPager.EV()
	d(ExternalVals)
end


function SORPager.TestLastZone()
	LastZone = 1
end

function SORPager.Test()
	SORPager.SepDetails(L" SoRData/5072:0:1:10:5073:1:1:140:5075:2:1:300:5074:3:1:0::")
	SORPager.SepDetails(L" SoRData/2:2:2:750:1:2:1:750:5099:2:1:750:5098:2:2:750::")
	SORPager.SepDetails(L" SoRData/228:2:1:750:229:2:1:750:5097:2:1:750:5096:2:1:750::")
end

local function timeformat(secs)
    local trunc = math.ceil(secs)
    local mins = math.floor((1 + trunc - (trunc % 60)) / 60)
    local secs = trunc % 60
	if mins > 60 then
		local hours = math.floor((1 + mins - (mins % 60)) / 60)
		local mins = mins % 60
	    return wstring.format(L"%d:%02d:%02d", hours, mins, secs)
	end
    return wstring.format(L"%d:%02d", mins, secs)
end

function SORPager.Initialize()
	RegisterEventHandler( SystemData.Events.CHAT_TEXT_ARRIVED, "SORPager.OnChat" )
	SORPager.SetText()
	
	for _, wndGroup in ipairs(EA_ChatWindowGroups) do 
		
		if wndGroup.used == true then
		
			for tabId, tab in ipairs(wndGroup.Tabs) do
				
				local tabName = EA_ChatTabManager.GetTabName( tab.tabManagerId )
				
				if tabName then
					if tab.tabText ~= L"SoR" then
						LogDisplaySetFilterState(tabName.."TextLog", "Chat", 6, false)
					else
						LogDisplaySetFilterState(tabName.."TextLog", "Chat", 6, true)
						LogDisplaySetFilterColor(tabName.."TextLog", "Chat", 6, 150, 129, 146 )
					end
				end
				
			end
			
		end

	end

end

function SORPager.LowBOsOn()
	local window = SystemData.ActiveWindow.name
	if SORData.KeeptoLabel[window] then
		window = SORData.KeeptoLabel[window].window
	end
	local dots = {}
	dots = SORData.LowMouseOvers[window]
	SOR.SetLabel("SOR.Pager.RealmText", L"Tier " .. towstring(dots[6]) .. L": " .. SORS.Pairings[dots[5]])
	for dot = 1,4 do
		WindowSetShowing("SOR.Pager" .. dot, false)
		WindowSetShowing("SOR.Pager.Realm" .. dot, true)
		if ExternalVals[dots[dot]] then
			if ExternalVals[dots[dot]].side == 1 then 
				DynamicImageSetTexture("SOR.Pager.Realm" .. dot .. ".Blip", "BlipOrd" , 8, 8)
			elseif ExternalVals[dots[dot]].side == 2 then
				DynamicImageSetTexture("SOR.Pager.Realm" .. dot .. ".Blip", "BlipDes" , 8, 8)
			else
				DynamicImageSetTexture("SOR.Pager.Realm" .. dot .. ".Blip", "" , 8, 8)
			end
			if ExternalVals[dots[dot]].known == 0 then
					DynamicImageSetTexture("SOR.Pager.Realm" .. dot .. ".Glow", "ObjUnknown" , 32, 32)
			elseif ExternalVals[dots[dot]].known == 1 then
				if ExternalVals[dots[dot]].side == 1 then 
					DynamicImageSetTexture("SOR.Pager.Realm" .. dot .. ".Glow", "ObjOrd" , 32, 32)
				elseif ExternalVals[dots[dot]].side == 2 then
					DynamicImageSetTexture("SOR.Pager.Realm" .. dot .. ".Glow", "ObjDes" , 32, 32)
				end	
			elseif ExternalVals[dots[dot]].known == 2 then
					DynamicImageSetTexture("SOR.Pager.Realm" .. dot .. ".Glow", "ObjLock" , 32, 32)
			elseif ExternalVals[dots[dot]].known == 3 then
				if ExternalVals[dots[dot]].side == 1 then 
					DynamicImageSetTexture("SOR.Pager.Realm" .. dot .. ".Glow", "LostOrd" , 32, 32)
				elseif ExternalVals[dots[dot]].side == 2 then
					DynamicImageSetTexture("SOR.Pager.Realm" .. dot .. ".Glow", "LostDes" , 32, 32)
				end						
			end
		else
			DynamicImageSetTexture("SOR.Pager.Realm" .. dot .. ".Glow", "" , 32, 32)
			DynamicImageSetTexture("SOR.Pager.Realm" .. dot .. ".Blip", "" , 8, 8)
		end
	end
end

function SORPager.BOsOff()
	for dot = 1,4 do
		WindowSetShowing("SOR.Pager" .. dot, true)
		WindowSetShowing("SOR.Pager.Realm" .. dot, false)
	end
	SOR.SetLabel("SOR.Pager.RealmText", L"")
end

function SORPager.HighBOsOn()
	local window = SystemData.ActiveWindow.name
	if SORData.KeeptoLabel[window] then
		window = SORData.KeeptoLabel[window].window
	end

	local dots = {}
	dots = SORData.HighMouseOvers[window]
	local pairData = GetCampaignPairingData( dots.pairing)
	local zone = SOR.ConvertZoneNumber(pairData.contestedZone, dots.pairing)
	SOR.SetLabel("SOR.Pager.RealmText", L"Tier 4: " .. towstring(GetZoneName(pairData.contestedZone)) )
	if zone == 1 or zone == 5 then
		zone = 3
	end
	dots = dots[zone]
	for dot = 1,4 do
		WindowSetShowing("SOR.Pager" .. dot, false)
		WindowSetShowing("SOR.Pager.Realm" .. dot, true)
		if ExternalVals[dots[dot]] then
			if ExternalVals[dots[dot]].side == 1 then 
				DynamicImageSetTexture("SOR.Pager.Realm" .. dot .. ".Blip", "BlipOrd" , 8, 8)
			elseif ExternalVals[dots[dot]].side == 2 then
				DynamicImageSetTexture("SOR.Pager.Realm" .. dot .. ".Blip", "BlipDes" , 8, 8)
			else
				DynamicImageSetTexture("SOR.Pager.Realm" .. dot .. ".Blip", "" , 8, 8)
			end
			if ExternalVals[dots[dot]].known == 0 then
					DynamicImageSetTexture("SOR.Pager.Realm" .. dot .. ".Glow", "ObjUnknown" , 32, 32)
			elseif ExternalVals[dots[dot]].known == 1 then
				if ExternalVals[dots[dot]].side == 1 then 
					DynamicImageSetTexture("SOR.Pager.Realm" .. dot .. ".Glow", "ObjOrd" , 32, 32)
				elseif ExternalVals[dots[dot]].side == 2 then
					DynamicImageSetTexture("SOR.Pager.Realm" .. dot .. ".Glow", "ObjDes" , 32, 32)
				end	
			elseif ExternalVals[dots[dot]].known == 2 then
					DynamicImageSetTexture("SOR.Pager.Realm" .. dot .. ".Glow", "ObjLock" , 32, 32)
			elseif ExternalVals[dots[dot]].known == 3 then
				if ExternalVals[dots[dot]].side == 1 then 
					DynamicImageSetTexture("SOR.Pager.Realm" .. dot .. ".Glow", "LostOrd" , 32, 32)
				elseif ExternalVals[dots[dot]].side == 2 then
					DynamicImageSetTexture("SOR.Pager.Realm" .. dot .. ".Glow", "LostDes" , 32, 32)
				end						
			end
		else
			DynamicImageSetTexture("SOR.Pager.Realm" .. dot .. ".Glow", "" , 32, 32)
			DynamicImageSetTexture("SOR.Pager.Realm" .. dot .. ".Blip", "" , 8, 8)
		end
	end
end

function SORPager.HideChannel(channelId)
	for _, wndGroup in ipairs(EA_ChatWindowGroups) do 
		if wndGroup.used == true then
			for tabId, tab in ipairs(wndGroup.Tabs) do
				local tabName = EA_ChatTabManager.GetTabName( tab.tabManagerId )
		
				if tabName then
					if tab.tabText ~= L"SoR" then
						LogDisplaySetFilterState(tabName.."TextLog", "Chat", channelId, false)
					else
						LogDisplaySetFilterState(tabName.."TextLog", "Chat", channelId, true)
						LogDisplaySetFilterColor(tabName.."TextLog", "Chat", channelId, 168, 187, 160 )
					end
				end
				
			end
			
		end
		
	end	
end
	


function SORPager.OnChat()
	local channel, channelId = SORPager.FindChannel()
	if channel and FirstLoad then
		SORPager.HideChannel(channelId)
		FirstLoad = false
	end
	if channel then
		if ( GameData.ChatData.type == 26 ) and ( GameData.ChatData.text:match(L"Slow down!") ) then
			SOR.Throttle()
			return
		end

		local msg = GameData.ChatData.text
		local author = GameData.ChatData.name

		if GameData.Player.name ~= GameData.ChatData.name 
		then
			local pos = msg:find(L"/")
			if pos and channelId then
				if msg:match(L"SoRData") and GameData.ChatData.type == channelId then
					SORPager.SepDetails(msg)
				end
			end
		else 
			SOR.Unthrottle()
		end
	end
end

function SORPager.SetText()
	-- WindowSetScale("SOR.Pager.RealmText", 0.65)
	-- for dot = 1,4 do
		-- WindowSetScale("SOR.Pager" .. dot .. ".Label", 0.65)
		-- WindowSetScale("SOR.Pager.Realm" .. dot .. ".Label", 0.65)
	-- end
	SOR.SetLabel("SOR.Pager1.Label", SORS.SORPager.Boons.AG)
	SOR.SetLabel("SOR.Pager2.Label", SORS.SORPager.Boons.MG)
	SOR.SetLabel("SOR.Pager3.Label", SORS.SORPager.Boons.DB)
	SOR.SetLabel("SOR.Pager4.Label", SORS.SORPager.Boons.HB)
	SOR.SetLabel("SOR.Pager.Realm1.Label", SORS.SORPager.Boons.AG)
	SOR.SetLabel("SOR.Pager.Realm2.Label", SORS.SORPager.Boons.MG)
	SOR.SetLabel("SOR.Pager.Realm3.Label", SORS.SORPager.Boons.DB)
	SOR.SetLabel("SOR.Pager.Realm4.Label", SORS.SORPager.Boons.HB)
	
end

function SORPager.SetTimes()
	for i = 1, 4 do
		if PagerVals[i] then
			if PagerVals[i].known == 0 then
				SOR.SetLabel("SOR.Pager" .. i .. ".Timer.Label", timeformat(SOR.GetUnknownTime()))
				SORPager.SetTimer(i, SOR.GetUnknownTime())
			end
		end
	end
end

function SORPager.SetTimerKnownBO(id, timeCount)
	if PagerVals[SORS.Info[id].objtype].id  == id then
		SOR.SetLabel("SOR.Pager" .. SORS.Info[id].objtype .. ".Timer.Label", timeformat(timeCount))
		SORPager.SetTimer(SORS.Info[id].objtype, timeCount)
	end
end


function SORPager.OnMouseOver()
  local window = SystemData.ActiveWindow.name
  local dot = tonumber(window:sub(10,10)) 
  if PagerVals[dot] then
	if PagerVals[dot].id ~= 666 then
	  if PagerVals[dot].known ~= 3 then
		  WindowSetShowing("SOR.Pager" .. dot .. ".Timer", true)	
	  end

	  Tooltips.CreateTextOnlyTooltip( window, nil ) 
	  Tooltips.SetTooltipText( 1, 1, PagerVals[dot].name)  
	  Tooltips.SetTooltipText( 2, 1, SORS.OBJBoon[dot])
	  if PagerVals[dot].side == 1 or PagerVals[dot].side == 2 then
		Tooltips.SetTooltipText( 3, 1, SORS.SORPager.controlled_by .. SORS.Side[PagerVals[dot].side])
	  else
		Tooltips.SetTooltipText( 3, 1, SORS.SORPager.not_controlled)
	  end
	  if PagerVals[dot].side == 1 then
		Tooltips.SetTooltipColor( 3, 1, 57, 141, 255)
	  elseif PagerVals[dot].side == 2 then
		Tooltips.SetTooltipColor( 3, 1, 255, 55, 55 )
	  else
		Tooltips.SetTooltipColor( 3, 1, 190, 190, 190 )
	  end
	  if PagerVals[dot].known ==  3 then
		Tooltips.SetTooltipText( 4, 1, SORS.SORPager.open_for_capture)
		Tooltips.SetTooltipColor( 4, 1, 255, 255, 255 )
	  elseif PagerVals[dot].known == 2 then
		Tooltips.SetTooltipText( 4, 1, SORS.SORPager.locked)
		Tooltips.SetTooltipColor( 4, 1, 190, 190, 190 )
	  elseif PagerVals[dot].known == 1 then
		Tooltips.SetTooltipText( 4, 1, SORS.SORPager.contested)
		Tooltips.SetTooltipColor( 4, 1, 255, 255, 255 )
	  else
		Tooltips.SetTooltipText( 4, 1, SORS.SORPager.status_unknown)
		Tooltips.SetTooltipColor( 4, 1, 190, 190, 190 )
	  end
	  Tooltips.Finalize()
	  if SOR_SavedSettings["TipLeft"] then
		Tooltips.AnchorTooltipManual("left" , "SOR.Pager" , "right", -2,0)
	  else
		Tooltips.AnchorTooltipManual("right" , "SOR.Pager" , "left", 2,0)
	  end
	end
  end
end

function SORPager.SetTimer(dot, timer)
	PagerVals[dot].timer = timer
end

function SORPager.SetTable(ObjType, ObjNo , ObjSide, ObjName)
	if ObjType then
		if PagerVals[ObjType].id == 666 then
			local PageObj = {}
			PageObj["id"] = ObjNo
			PageObj["side"] = ObjSide
			PageObj["name"] = ObjName
			PageObj["known"] = 0
			PageObj["timer"] = 0
			PagerVals[ObjType] = PageObj
			DynamicImageSetTexture("SOR.Pager" .. ObjType .. ".Glow", "ObjUnknown" , 32, 32)
			if ObjSide == 1 then 
			DynamicImageSetTexture("SOR.Pager" .. ObjType .. ".Blip", "BlipOrd" , 8, 8)
			elseif ObjSide == 2 then
			DynamicImageSetTexture("SOR.Pager" .. ObjType .. ".Blip", "BlipDes" , 8, 8)
			end
			BroadcastPager[ObjType] = SORS.OBJBoon[ObjType] .. L": " .. PagerVals[ObjType].name .. 
										SORS.SORPager.is_controlled_by .. SORS.Side[PagerVals[ObjType].side] ..
										SORS.SORPager.but_i_dont_have_a_timer_on_it
			
			local side = SORObjs.IsLockedBO(ObjNo)		
			if side then
				if side == ObjSide then
					SORPager.Locked(ObjNo, ObjSide)
				else
					SORObjs.RemoveLockedBO(ObjNo)
				end
			elseif ExternalVals[ObjNo] then
				if ExternalVals[ObjNo].side == ObjSide then
					if ExternalVals[ObjNo].known == 1 then
						SORPager.Contested(ObjNo, ObjSide)
					elseif ExternalVals[ObjNo].known == 2 then
						SORPager.Locked(ObjNo, ObjSide)
					elseif ExternalVals[ObjNo].known == 3 then
						SORPager.Known(ObjNo)
					end
				end
			end
		end
	end
end

function SORPager.CheckZone()
	local getzone = 0
	if SORObjs.GetTier() == 4 and not (GameData.Player.isInScenario) then
		local Pairing = GetZonePairing()
		if Pairing and Pairing >= 1 and Pairing <= 3 then
			pairData  = GetCampaignPairingData(Pairing)
			getzone = SOR.ConvertZoneNumber( pairData.contestedZone, Pairing)
			if getzone == 1 or getzone == 5 then
				getzone = 3
			end
		end
	end
	if getzone ~= LastZone and LastZone ~= 0 and getzone ~= 0 then
		if ZoneChangeThrottle >= 2 then
			SORObjs.SelectPagerIds()
			LastZone = getzone	
			ZoneChangeThrottle = 0
		else
			ZoneChangeThrottle = ZoneChangeThrottle + 1
		end
	end
end

function SORPager.Reset()
		SOR.SetUnknownTime(1080)
		local PageObj = {}
		for i = 1, 4 do
			DynamicImageSetTexture("SOR.Pager" .. i .. ".Blip", "" , 8, 8)
			DynamicImageSetTexture("SOR.Pager" .. i .. ".Glow", "" , 32, 32)
			BroadcastPager[i] = L"?"	
				PageObj["id"] = 666
				PageObj["side"] = 3
				PageObj["name"] = SORS.SORPager.no_bos_in_this_zone
				PageObj["known"] = 0
				PagerVals[i] = PageObj
		end
end

function SORPager.FindChannel()
    for k, v in pairs(ChatSettings.Channels) do
        if ( v.labelText ~= nil ) then
            if ( v.labelText:match(SORData.Channel[GameData.Player.realm]) ) then
				return v.serverCmd ,k
            end
        end
    end
    
    return false, false
end

function SORPager.UnLogChannel()
	if SOR_SavedSettings["NoUpdate"] then
		SystemData.UserInput.ChatText = L"/channelleave "
		.. SORData.Channel[GameData.Player.realm]
		BroadcastEvent( SystemData.Events.SEND_CHAT_TEXT )
	end
end

function SORPager.LogChannel()
	if not SOR_SavedSettings["NoUpdate"] then
		local Channel , ChannelId = SORPager.FindChannel()
		if not Channel then
			SystemData.UserInput.ChatText = L"/channel " ..
			SORData.Channel[GameData.Player.realm]
			BroadcastEvent( SystemData.Events.SEND_CHAT_TEXT )
		end
	end
end
	
function SORPager.SendDetails()
		SORPager.LogChannel()
		local Channel, ChannelId = SORPager.FindChannel()
		if Channel then
			local sendtext = L" SoRData/"
			for k,v in ipairs(PagerVals) do
				if v.id ~= 666 then
				sendtext = sendtext .. towstring(v.id) .. L":"
				.. towstring(v.known) .. L":" .. towstring(v.side) .. L":" .. towstring(math.ceil(v.timer)) .. L":"
				
					local BOInfo = {}
					BOInfo["name"] = v.name
					BOInfo["known"] = v.known
					BOInfo["side"] = v.side
					BOInfo["timer"] = math.ceil(v.timer)
					BOInfo["logtime"] = GetComputerTime()		
					ExternalVals[v.id] = BOInfo
				
				else
					return
				end
			end
			if #tostring(sendtext) > 16 then
				SystemData.UserInput.ChatText = Channel .. sendtext .. L":"
				BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)
			end
		end
end

function SORPager.RemoveOld()
		local RemoveVals = {}
		local doneindex = 0
		for k,v in pairs(ExternalVals) do
			if SORS.Info[k].tier == 1 and GetComputerTime() - v.logtime > 60 then
				if v.known == 1 then
					SORObjs.RemoveContestedBO(k)
				end
				doneindex = doneindex + 1
				RemoveVals[doneindex ] = k
			elseif GetComputerTime() - v.logtime > 30 then
				if v.known == 1 then
					SORObjs.RemoveContestedBO(k)
				end
				doneindex = doneindex + 1
				RemoveVals[doneindex ] = k
			end
		end
		for k = 1, #RemoveVals do
			ExternalVals[RemoveVals[k]] = nil
		end
end

function SORPager.SepDetails(text)
	if #tostring(text) > 16 then
	local ResetTime = false
	local pos = text:find(L"/")
		for i=1,4 do
			local BOInfo = {}
			text = text:sub(pos + 1)	
			pos = text:find(L":")
			local id = tonumber(text:sub(1,pos - 1))
				BOInfo["name"] = SORS.Info[id].name
			text = text:sub(pos + 1)	
			pos = text:find(L":")
				BOInfo["known"] = tonumber(text:sub(1,pos - 1))
			text = text:sub(pos + 1)	
			pos = text:find(L":")
				BOInfo["side"] = tonumber(text:sub(1,pos - 1))
			text = text:sub(pos + 1)	
			pos = text:find(L":")
				BOInfo["timer"] = tonumber(text:sub(1,pos - 1))
				BOInfo["logtime"] = GetComputerTime()
							
				if PagerVals[i] then
					if PagerVals[i].id ~= id then
						if ExternalVals[id] then
							if BOInfo["known"] ~= ExternalVals[id].known or BOInfo["side"] ~= ExternalVals[id].side then
								if BOInfo["known"] == 1 then
									SORObjs.AddContestedBO(id, BOInfo["side"], BOInfo["timer"])
								elseif BOInfo["known"] == 2 then
									SORObjs.AddLockedBO(id, BOInfo["side"], BOInfo["timer"])							
								end
							end
						else
							if BOInfo["known"] == 1 then
								SORObjs.AddContestedBO(id, BOInfo["side"], BOInfo["timer"])
							elseif BOInfo["known"] == 2 then
								SORObjs.AddLockedBO(id, BOInfo["side"], BOInfo["timer"])
							end	
						end
					else
						if (PagerVals[i].known == 0 and BOInfo["side"] == PagerVals[i].side) or PagerVals[i].side == 0 then
							if BOInfo["known"] == 0 then
								SOR.CheckUnknownTime(BOInfo["timer"])
							elseif BOInfo["known"] == 1 then
								SORObjs.AddContestedBO(id, BOInfo["side"], BOInfo["timer"])
							elseif BOInfo["known"] == 2 then
								SORObjs.AddLockedBO(id, BOInfo["side"], BOInfo["timer"])
							elseif BOInfo["known"] == 3 then
								SORPager.Known(id)
							end							
						end
						ResetTime = true
					end
				end
				
			ExternalVals[id] = BOInfo
		end
		if ResetTime then SOR.AddTime() end
	end
end

function SORPager.Switch()
	for i = 1, 4 do
		if PagerVals[i].known == 0 then
				PagerVals[i].known = 3
				PagerVals[i].timer = 0
				if PagerVals[i].side == 1 then 
				DynamicImageSetTexture("SOR.Pager" .. i .. ".Glow", "LostOrd" , 32, 32)
				BroadcastPager[i] = SORS.OBJBoon[i] .. L": " .. PagerVals[i].name .. 
								SORS.SORPager.open_for_attack_by_the_forces_of .. SORS.Side[2]
				elseif PagerVals[i].side == 2 then
				DynamicImageSetTexture("SOR.Pager" .. i .. ".Glow", "LostDes" , 32, 32)
				BroadcastPager[i] = SORS.OBJBoon[i] .. L": " .. PagerVals[i].name .. 
								SORS.SORPager.is_open_for_attack_by_the_forces_of .. SORS.Side[1]
				end		
		end
	end

end

function SORPager.Cleared(ObjNo)
	if PagerVals[SORS.Info[ObjNo].objtype].id == ObjNo then
		PagerVals[SORS.Info[ObjNo].objtype].side  = 0
		PagerVals[SORS.Info[ObjNo].objtype].known  = 0
		PagerVals[SORS.Info[ObjNo].objtype].timer = 0
		DynamicImageSetTexture("SOR.Pager" .. SORS.Info[ObjNo].objtype .. ".Blip", "" , 8, 8)
		DynamicImageSetTexture("SOR.Pager" .. SORS.Info[ObjNo].objtype .. ".Glow", "" , 32, 32)
	end
end

function SORPager.Contested(ObjNo, ObjSide)		
	if PagerVals[SORS.Info[ObjNo].objtype].id == ObjNo then
		PagerVals[SORS.Info[ObjNo].objtype].side  = ObjSide
		PagerVals[SORS.Info[ObjNo].objtype].known  = 1	
		if ObjSide == 1 then 
		DynamicImageSetTexture("SOR.Pager" .. SORS.Info[ObjNo].objtype .. ".Blip", "BlipOrd" , 8, 8)
		DynamicImageSetTexture("SOR.Pager" .. SORS.Info[ObjNo].objtype .. ".Glow", "ObjOrd" , 32, 32)
		elseif ObjSide == 2 then
		DynamicImageSetTexture("SOR.Pager" .. SORS.Info[ObjNo].objtype .. ".Blip", "BlipDes" , 8, 8)
		DynamicImageSetTexture("SOR.Pager" .. SORS.Info[ObjNo].objtype .. ".Glow", "ObjDes" , 32, 32)
		end

	end
end

function SORPager.SetBCContested(ObjNo, Time)
	if PagerVals[SORS.Info[ObjNo].objtype].id == ObjNo then
		BroadcastPager[SORS.Info[ObjNo].objtype] = L"Tier " .. SORS.Info[ObjNo].tier .. L" - " .. SORS.OBJBoon[SORS.Info[ObjNo].objtype] .. L": "
						.. PagerVals[SORS.Info[ObjNo].objtype].name .. SORS.SORPager.will_be_locked_down_by
						.. SORS.Side[PagerVals[SORS.Info[ObjNo].objtype].side] .. SORS.SORPager._in
						.. Time .. SORS.SORPager.minutes
	end
end

function SORPager.SetBCLocked(ObjNo, Time)
	if PagerVals[SORS.Info[ObjNo].objtype].id == ObjNo then
		BroadcastPager[SORS.Info[ObjNo].objtype] = L"Tier " .. SORS.Info[ObjNo].tier .. L" - " .. SORS.OBJBoon[SORS.Info[ObjNo].objtype] .. L": "
						.. PagerVals[SORS.Info[ObjNo].objtype].name .. SORS.SORPager.is_locked_by
						.. SORS.Side[PagerVals[SORS.Info[ObjNo].objtype].side] .. SORS.SORPager.for_another
						.. Time .. SORS.SORPager.minutes
	end
end

function SORPager.Locked(ObjNo, ObjSide)
	if PagerVals[SORS.Info[ObjNo].objtype].id == ObjNo then
		PagerVals[SORS.Info[ObjNo].objtype].side  = ObjSide
		PagerVals[SORS.Info[ObjNo].objtype].known  = 2
		
		DynamicImageSetTexture("SOR.Pager" .. SORS.Info[ObjNo].objtype .. ".Glow", "ObjLock" , 32, 32)
		if ObjSide == 1 then 
		DynamicImageSetTexture("SOR.Pager" .. SORS.Info[ObjNo].objtype .. ".Blip", "BlipOrd" , 8, 8)
		elseif ObjSide == 2 then
		DynamicImageSetTexture("SOR.Pager" .. SORS.Info[ObjNo].objtype .. ".Blip", "BlipDes" , 8, 8)
		end
	end
end

function SORPager.Known(ObjNo)
	if PagerVals[SORS.Info[ObjNo].objtype].id == ObjNo then
		PagerVals[SORS.Info[ObjNo].objtype].known = 3
		PagerVals[SORS.Info[ObjNo].objtype].timer = 0
		WindowSetShowing("SOR.Pager" .. SORS.Info[ObjNo].objtype .. ".Timer", false)
		if PagerVals[SORS.Info[ObjNo].objtype].side == 1 then 
			DynamicImageSetTexture("SOR.Pager" .. SORS.Info[ObjNo].objtype .. ".Blip", "BlipOrd" , 8, 8)
			DynamicImageSetTexture("SOR.Pager" .. SORS.Info[ObjNo].objtype .. ".Glow", "LostOrd" , 32, 32)
		elseif PagerVals[SORS.Info[ObjNo].objtype].side == 2 then
			DynamicImageSetTexture("SOR.Pager" .. SORS.Info[ObjNo].objtype .. ".Blip", "BlipDes" , 8, 8)
			DynamicImageSetTexture("SOR.Pager" .. SORS.Info[ObjNo].objtype .. ".Glow", "LostDes" , 32, 32)
		end
    end
end


function SORPager.HidePopup()
	for i = 1, 4 do
		WindowSetShowing("SOR.Pager" .. i .. ".Timer", false)		
	end
end

function SORPager.OnLClick()
  local window = SystemData.ActiveWindow.name
  local dot = tonumber(window:sub(10,10)) 
  if PagerVals[dot].id ~= 666 then
	  if PagerVals[dot] then
		EA_ChatWindow.InsertText(PagerVals[dot].name .. L" (" .. SORS.OBJBoon[dot] .. L") " )
	  end
	end
end

function SORPager.ShowPagerMenu()
	local window = SystemData.ActiveWindow.name
	local parent = window:sub(1,10)
	local dot = tonumber(window:sub(10,10))
	if PagerVals[dot].id ~= 666 then
		BroadcastPager[5] = dot
		
		EA_Window_ContextMenu.CreateContextMenu(parent, EA_Window_ContextMenu.BARMENU)
		
		local channel = SOR.GetChannel()
	 
	 	if SOR_SavedSettings["CustomChat"] then
			EA_Window_ContextMenu.AddMenuItem(SORS.Chats.send_target .. SORS.Chats._to .. SOR_SavedSettings["CustomChat"], SORPager.BroadcastDC, false, true, EA_Window_ContextMenu.BARMENU)
		end
	
		if channel == 2 then 
			EA_Window_ContextMenu.AddMenuItem(SORS.Chats.send_target .. SORS.Chats._to .. SORS.Chats.warband, SORPager.BroadcastDW, false, true, EA_Window_ContextMenu.BARMENU)
		elseif channel == 1 then
			EA_Window_ContextMenu.AddMenuItem(SORS.Chats.send_target .. SORS.Chats._to .. SORS.Chats.party, SORPager.BroadcastDP, false, true, EA_Window_ContextMenu.BARMENU)
		end	
		if SOR.IsChannel(1) then
		EA_Window_ContextMenu.AddMenuItem(SORS.Chats.send_target .. SORS.Chats._to .. SORS.Chats.region, SORPager.BroadcastD1, false, true, EA_Window_ContextMenu.BARMENU)
		end
		if SOR.IsChannel(2) then
		EA_Window_ContextMenu.AddMenuItem(SORS.Chats.send_target .. SORS.Chats._to .. SORS.Chats.rvr, SORPager.BroadcastD2, false, true, EA_Window_ContextMenu.BARMENU)
		end

	 	if SOR_SavedSettings["CustomChat"] then
			EA_Window_ContextMenu.AddMenuItem(SORS.Chats.broadcast_info .. SORS.Chats._to .. SOR_SavedSettings["CustomChat"], SORPager.BroadcastC, false, true, EA_Window_ContextMenu.BARMENU)
		end
		
		if channel == 2 then 
			EA_Window_ContextMenu.AddMenuItem(SORS.Chats.broadcast_info .. SORS.Chats._to .. SORS.Chats.warband, SORPager.BroadcastW, false, true, EA_Window_ContextMenu.BARMENU)
		elseif channel == 1 then
			EA_Window_ContextMenu.AddMenuItem(SORS.Chats.broadcast_info .. SORS.Chats._to .. SORS.Chats.party, SORPager.BroadcastP, false, true, EA_Window_ContextMenu.BARMENU)
		end
		if SOR.IsChannel(1) then
		EA_Window_ContextMenu.AddMenuItem(SORS.Chats.broadcast_info .. SORS.Chats._to .. SORS.Chats.region, SORPager.Broadcast1, false, true, EA_Window_ContextMenu.BARMENU)
		end
		if SOR.IsChannel(2) then
		EA_Window_ContextMenu.AddMenuItem(SORS.Chats.broadcast_info .. SORS.Chats._to .. SORS.Chats.rvr, SORPager.Broadcast2, false, true, EA_Window_ContextMenu.BARMENU)
		end
		
		EA_Window_ContextMenu.Finalize(EA_Window_ContextMenu.BARMENU)
	end
end


function SORPager.BroadcastDC()
	SystemData.UserInput.ChatText = L"/" .. SOR_SavedSettings["CustomChat"] .. SORS.SORPager.moving_on_to .. SORS.OBJBoon[BroadcastPager[5]] 
								.. L": " .. PagerVals[BroadcastPager[5]].name .. SORS.SORPager._next
	BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)
end

function SORPager.BroadcastD1()
	SystemData.UserInput.ChatText = L"/1 " .. SORS.SORPager.moving_on_to .. SORS.OBJBoon[BroadcastPager[5]] 
								.. L": " .. PagerVals[BroadcastPager[5]].name .. SORS.SORPager._next
	BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)
end

function SORPager.BroadcastD2()
	SystemData.UserInput.ChatText = L"/2 " .. SORS.SORPager.moving_on_to .. SORS.OBJBoon[BroadcastPager[5]] 
								.. L": " .. PagerVals[BroadcastPager[5]].name .. SORS.SORPager._next
	BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)
end

function SORPager.BroadcastDW()
	SystemData.UserInput.ChatText = L"/war " .. SORS.SORPager.moving_on_to .. SORS.OBJBoon[BroadcastPager[5]]
								.. L": " .. PagerVals[BroadcastPager[5]].name .. SORS.SORPager._next
	BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)
end

function SORPager.BroadcastDP()
	SystemData.UserInput.ChatText = L"/p " .. SORS.SORPager.moving_on_to .. SORS.OBJBoon[BroadcastPager[5]]
								.. L": " .. PagerVals[BroadcastPager[5]].name .. SORS.SORPager._next
	BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)
end

function SORPager.BroadcastC()
	SystemData.UserInput.ChatText = L"/" .. SOR_SavedSettings["CustomChat"] .. L" " .. BroadcastPager[BroadcastPager[5]]
	BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)
end

function SORPager.Broadcast1()
	SystemData.UserInput.ChatText = L"/1 " .. BroadcastPager[BroadcastPager[5]]
	BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)
end

function SORPager.Broadcast2()
	SystemData.UserInput.ChatText = L"/2 " .. BroadcastPager[BroadcastPager[5]]
	BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)
end

function SORPager.BroadcastW()
	SystemData.UserInput.ChatText = L"/war " .. BroadcastPager[BroadcastPager[5]]
	BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)
end

function SORPager.BroadcastP()
	SystemData.UserInput.ChatText = L"/p " .. BroadcastPager[BroadcastPager[5]]
	BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)
end

